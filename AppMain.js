/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StatusBar, StyleSheet, Text, View, Navigator, Image } from 'react-native';
import Home from './components/Home/Main'
import Cart from './components/Cart/Main'

import { StackNavigator, TabNavigator, ThemeProvider, uiTheme, Toolbar } from 'react-navigation'
import Notifi from './components/Notifi/SecondScreen'
import Login from './components/Login/LoginScreen'
import Register from './components/Register/Register'
import App from './App';
import Info from './components/Info/Info';
import ls from 'react-native-local-storage';



const MystackNavigator = StackNavigator({
    stackInfo: { screen: Info },
    // stackLogin: { screen: FoodDetail },
   

});

// ls.get('email').then((email) => {
//     // alert(email.trim().length+"dknlknf")
//     if (email.trim().length > 0) {
        
//          this.props.navigation.navigate('Login');
//     } else {
        
//     }



const AppMain = TabNavigator({
    Cart: {
        screen: Cart,
        navigationOptions: {

            tabBarLabel: "Tab 1",
            actionButton: {
                onPress: () => {
                    // alert('ok') 
                }
            },
            // tabBarIcon: ({ tintColor, focused }) => <Image
            //     source={focused ? require('./components/Images/cartactive.png') : require('./components/Images/cart.png')}
            //     style={{
            //         width: 24, height: 24,
            //         //tintColor: focused ? 'green' : 'gray',
            //     }}
            // />
        }
    },
    Home: {

        screen:(props)=> <Home {...props} />,
        navigationOptions: {
           
            tabBarLabel: "Tab 1",
            actionButton:{
                onPress: () => {
                    // alert('ok')
                }
            },
            tabBarIcon: ({ tintColor, focused }) => <Image
                source={focused ? require('./components/Images/homeactive.png') : require('./components/Images/home.png')}
                style={{
                    width: 24, height: 24,
                    //tintColor: focused ? 'green' : 'gray',
                }}
            />
        }
    },
    
    Notifi: { screen: Notifi,
        navigationOptions: {

            tabBarLabel: "Tab 1",
            actionButton: {
                onPress: () => { 
                    // alert('ok') 
                }
            },
            tabBarIcon: ({ tintColor, focused }) => <Image
                source={focused ? require('./components/Images/bellactive.png') : require('./components/Images/bell.png')}
                style={{
                    width: 24, height: 24,
                    //tintColor: focused ? 'green' : 'gray',
                }}
            />
        }
    },

    Info: {
        screen: props => <Info {...props} />,
        navigationOptions: {

            tabBarLabel: "Tab 1",
            actionButton: {
                onPress: () => { 
                    // alert('ok')
                 }
            },
            tabBarIcon: ({ tintColor, focused }) => <Image
                source={focused ? require('./components/Images/infoactive.png') : require('./components/Images/info.png')}
                style={{
                    width: 24, height: 24,
                    //tintColor: focused ? 'green' : 'gray',
                }}
            />
        }
    },
    
},
    {
        // tabBarPosition: 'Bottom',
        swipeEnabled: false,

        // ...TabNavigator.Presets.AndroidTopTabs,
        tabBarOptions: {
            // scrollEnabled: true,
            showIcon: true,
            showLabel:false,
            
            activeTintColor: '#ff0000',
          //  activeBackgroundColor: 'rgb(29, 144, 175)',
            animationEnabled: true,
            labelStyle: {
                color: 'black',
                fontSize: 16,
                fontWeight: 'bold',
            },

            tabStyle: {
                 width: 100,
                 //borderLeftWidth: 1,
                 //borderRightWidth: 1,
                
            },

            style: {
                // width: 600,
                backgroundColor: 'white',
               // color:'black'
            },
        }
    })
export default AppMain;
