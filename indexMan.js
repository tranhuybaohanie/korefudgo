import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    
} from 'react-native';
import App from './App'
import { Actions, ActionConst } from 'react-native-router-flux';
import ls from 'react-native-local-storage'
import * as actionRedux from './Storage/Actions/Index';
import { connect } from 'react-redux';

const SIZE = 80;
class indexMan extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            opacity: new Animated.Value(0)
        };
        ls.get('Lang').then((lang) => {
        if(lang=="en"||lang=="vi"){
            this.setLang(lang)
         }else{
            this.setLang("en")
        }
        }).catch(()=>{
         
            this.setLang("en")
        })

        ls.get('Cart').then((itemList) => {
            if (typeof itemList == "object" && itemList.length>0) {
                this.props.UpdateCart([{
                    itemList: itemList
                }])
            } else{
                this.props.UpdateCart([{
                    itemList: []
                }])
            }
        }).catch(() => {
            this.props.UpdateCart([{
                itemList: []
            }])
        })
      
    }
    setLang = (lang) => {
        var obLang = [{ lang: lang }]
        // if (this.props.Language != this.state.lang) {
            this.props.updateLang(obLang)
            // localStorage.setItem("lang", lang);
        // }

    }
    componentDidMount(){
        Animated.timing(
            this.state.opacity,
            {
                toValue:1,
                duration:2000
            }
        ).start();
             ls.get('email').then((email) => {
                // alert(email.trim().length+"dknlknf")
                if (email.trim().length > 0) {
                    rel = true;
                  
                    setTimeout(()=>{
                   this.props.navigation.navigate('AppMain');
                    },0)
                } else {
                   
                    setTimeout(() => {
                        this.props.navigation.navigate('Login');
                    }, 2000)
                }
             
            }).catch((e)=>{
               this.props.navigation.navigate('Login');
            });
         

        
    }
    render() {
       var {opacity} = this.state;
        return (
         <Animated.View style={{backgroundColor:'black',opacity}}
         
         ><Text style={{fontSize:20}}>HI</Text></Animated.View>
        );
    }
}


const mapStateToProps = state => {
    return {
        Lang: state.Language
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        //   setToDB: () => {
        //     dispatch(actFetchHotelsRequest());
        //   }
        updateLang: (Lang) => {

            dispatch(actionRedux.updateLanguage(Lang))
        },
        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(indexMan);