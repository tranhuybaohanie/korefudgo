import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import ButtonSubmit from './ButtonSubmit';
import SignupSection from './SignupSection';
import { firebaseApp } from '../../utlils/firebaseConfig';
import { Actions, ActionConst } from 'react-native-router-flux';
import { StyleSheet, View, StatusBar, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Dimensions from 'Dimensions';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
export default class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      email: "",
      passWord: "",
      passWord2:"",
    }
   
  }

  static navigationOptions = { header: null }

  register = () => {
    if (this.state.userName==""|| this.state.email==""|| this.state.passWord==""){
      alert("Please fill all input fields!")
    }else{
      if(this.state.passWord!=this.state.passWord2){
        alert("Please, comparing your password & repeat password!!")
      }else{

    actionRedux.createUserCustomer(this.state.userName,this.state.email, this.state.passWord,(result)=>{
      if(result==true){
        alert("Hi!  you create successful your account.");
        this.props.navigation.navigate("Login")
      }else{
        alert(result)
      }

    })
  }
  }
    // firebaseApp.auth().createUserWithEmailAndPassword(this.state.userName, this.state.passWord)
    //   .then((data) => {
      
    //   })
    //   .catch(function (error) {
    //     // Handle Errors here.
    //     var errorCode = error.code;
    //     var errorMessage = error.message;
    //     alert(errorMessage);
    //     // ...
    //   });
  }

  onChangeState = (key, value) => {
    if (key == "userName" && this.state.userName != value) {
      this.setState({
        [key]: value
      });
    }
    if (key == "passWord" && this.state.passWord != value) {
      this.setState({
        [key]: value
      });
    }
    if (key == "passWord2" && this.state.passWord != value) {
      this.setState({
        [key]: value
      });
    }
      if (key == "email" && this.state.email != value) {
        this.setState({
          [key]: value
        });
      //  alert(this.state.passWord); 

    }


  }
  onFocusInput = () => {
    // alert("ok")
  }
  render() {
    const navigation = this.props.navigation;
    return (
      <Wallpaper>
        {/* */}
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView style={styles.container}>
            <View style={styles.containerSmall} >
              <StatusBar barStyle='light-content'></StatusBar>
              <Logo />
              <Form onChangeState={this.onChangeState} />


              <ButtonSubmit register={this.register} navigation={navigation}/>
            </View>

          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
        {/*  */}
      </Wallpaper>

    );
  }
}
const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null,
   
    height: 500
    // (DEVICE_HEIGHT/100)*90
    // resizeMode: 'cover',
  },
  containerSmall: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: '#f722657a',
    margin:10,
    height: 500
    // (DEVICE_HEIGHT/100)*90
    // resizeMode: 'cover',
  },
});