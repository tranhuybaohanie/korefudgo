import React, {Component} from 'react';
// import PropTypes from 'prop-types';
import {StyleSheet, View, Text, Image} from 'react-native';

import logoImg from '../Images/fav-icon.png';
// import Font, { Fonts } from '../../utlils/fonts';

import PoiretOneRegular from '../../assets/fonts/PoiretOne-Regular.ttf'

export default class Logo extends Component {

constructor(props){
  super(props);
  this.state={
    isLoad:false
  }
}
//  componentDidMount(){
//   setTimeout(()=>{
//      Font.loadAsync({
//       PoiretOneRegular
//   //  'Prata':require
//   }).then(()=>{
//     // console.log(Font.processFontFamily);
// this.setState({
//   isLoad:true
// })  })
// alert("ok"+this.state.isLoad)

// },0)
// }

  render() {
   
    return (
      <View style={styles.container}>
        <Image source={logoImg} style={styles.image} />
        <Text  style={[styles.text,this.state.isLoad?{fontFamily:'AmericanTypewriter-Light'}:{} ] }>Korefud</Text>
        <Text  style={styles.textCap}>Create your awesome account</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 80,
    height: 80,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 40,
    backgroundColor: 'transparent',
    marginTop: 10,
    fontFamily:"AvenirNext-Heavy"
  },
  textCap:{
    color: 'gray',
    fontWeight: 'bold',
    fontFamily:'AvenirNext-Heavy',
    fontSize: 15,
    
    backgroundColor: 'transparent',
    
  }
});
