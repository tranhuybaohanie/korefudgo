import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import {Actions, ActionConst} from 'react-native-router-flux';
import ls from 'react-native-local-storage';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';



export default class Info extends Component {

  constructor(props) {
    super(props);
 

    this.state = {
      isLoading: false,
    };

  
  }


  _onPress= ()=> {
     ls.save('email', "")
      .then(() => {
        ls.get('email').then((data) => { 
         
         
        
        });
        // output should be "get: Kobe Bryant"
      })
      
    this.props.navigation.navigate('Login');
   
  }

  render() {
    

    return (
      <View style={styles.container}>
      <View style={{
        flex:1,
       // backgroundColor:black,
        alignContent:'center',
        justifyContent:'center'
      }}>
      
      <Text>This is info</Text>
      </View>
        <TouchableOpacity
          onPress={this._onPress}
          style={styles.button}
          activeOpacity={0.4}>
      <Text style={{color:'white'}}>Logout</Text>
        </TouchableOpacity>
       
      </View>
    );
  }
}
const HeightBTN = 50;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 350,
    height: HeightBTN,
    borderRadius: 7,
    zIndex: 99,
    backgroundColor: 'red',
    color: 'white',
    fontSize:15,
    marginBottom:5
  },
  circle: {
    height: HeightBTN,
    width: HeightBTN,
    marginTop: -HeightBTN,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});
