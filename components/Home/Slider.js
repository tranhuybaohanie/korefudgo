import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    ImageBackground,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Animated,
    Easing,
    FlatList,
    Dimensions
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient'
import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index'
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';

const SIZE = 80;


 class Slider extends Component {

    constructor(props) {
        super(props);
        this.state = {
            Language: "vi",
            itemSlider: this.props.itemsSlider||[],
            itemSliderComponent: this.props.itemsSliderComponent||[]
        }
        
    }
  
    componentWillReceiveProps(newProps) {
    
        var previousLang = this.props.Lang[0]["lang"];
        if (this.props != newProps) {
            this.setState({
                Language: newProps.Lang[0]["lang"],
            });

        }
       
        if (newProps.itemsSlider){

            this.setState({ 
                itemSliderComponent: newProps.itemsSliderComponent,
                itemSlider: newProps.itemsSlider
            },()=>{
                
            })

    }


    }
    stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail=(item)=>{
      this.props.navigate("FoodIDetail",{item})
    }
     showSlider = (itemSliderComponent) => {

        return itemSliderComponent.map((item, id) => {
       
            var lang = this.state.Language;
            var { name_vi,name_en, bigCategoryName, description_vi,description_en, img_url } = item
            // if(bigCategoryName=="Food"){
            var name=eval("name_"+lang)
            var description = eval("description_" + lang)
            return (
                <TouchableWithoutFeedback onPress={()=>{this.showDetail(item)}} >
                <ImageBackground source={{ uri: img_url }}  blurRadius={0} style={{ ...styles.containt }} key={id}>
                   
                    <View style={styles.contentSlider}> 
                    <LinearGradient
                                colors={['#65656500', '#656565c9']}
          start={{ x: 0.0, y: 0.0 }} end={{ x: 0.0, y: 1.0 }}
            style={{ height: 69, width: WDevice,position:"absolute"}}
        ></LinearGradient>
                    
                        <View style={{height:50,borderRadius:10,marginLeft:10}}>
                            <Image style={{ ...styles.slide1, width: 50, height: 58 ,borderRadius:10}} source={{ uri: img_url }} ></Image>
                        </View>
                        <View style={styles.comboText}>
                            <Text style={styles.text}>{name}</Text>
                            <Text style={styles.textDescription}>{description}</Text>
                        </View>
                    </View>
                   
                </ImageBackground>
                </TouchableWithoutFeedback>
            
        )

    })
}
    render() {
    
        var h = this.showSlider(this.state.itemSliderComponent)

        return (

            <Swiper style={styles.wrapper} showsButtons={false} activeDotColor="white"
                autoplay={true} loop={true} paginationStyle={styles.dot}
                key={this.state.itemSliderComponent.length}
            >
                {/* {this.showSlider()} */}
                {h}
            </Swiper>
        );
    }
}



const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
    dot: {

        marginLeft: 290,
        marginBottom: 150

    },
    wrapper: {

        height: 200
    },
    contentSlider: {
        // flex: 1,
        height:70,
        width: WDevice,
   
        // backgroundColor:"black",
        backgroundColor:"#50525229",
        alignItems: "center",
        flexDirection: 'row'

    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',

    },
    containt: {
        flex: 1,
        height:200,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    comboText:{
        marginLeft:5
    },  
      text: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
    },
    textDescription: {
        color: '#fff',
        fontSize: 13,
        
    }
})



const mapStateToProps = state => {
    return {
        Lang: state.Language,

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Slider);