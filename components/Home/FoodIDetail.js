import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    ScrollView
} from 'react-native';
import Swiper from 'react-native-swiper';
import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;
class FoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: "en",
            item: this.props.navigation.getParam('item'),
            imgMoreCount: 0
        }
    }
    componentDidMount() {
        // actionRedux.getProductByID(this.props.item.item, itemMenu=>{
        //     this.setState({itemMenu})
        // })
        const { navigation: { setParams } } = this.props;
        setParams({
            title: this.state.Language == "en" ? "Detail" : "Chi tiết"
        });
    }
    componentWillReceiveProps(newprops) {

        var previousLang = this.props.Lang[0]["lang"];
        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });
            const { navigation: { setParams } } = this.props;
            setParams({
                title: newprops.Language == "en" ? "Detail" : "Chi tiết"
            });


        }
    }
    static navigationOptions = ({ navigation }) => {


        return {
            title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'Food' : navigation.state.params.title,
            headerStyle: {
                backgroundColor: 'white',


            }, navigationOptions: {
                header: {
                    style: {
                        shadowOpacity: 0,
                        shadowOffset: {
                            height: 0,
                            width: 0
                        },
                        shadowRadius: 0,
                    }
                }
            }
        }
    }
    getImgMore = (imgMore) => {
        var imgMoreArr = imgMore.split("{bao&%/break}");
        if (this.state.imgMoreCount != imgMore.length)
            this.setState({ imgMoreCount: imgMore.length })
        return imgMoreArr.map((item, idex) => {

            return (
                <Image source={{ uri: item }} style={{ width: WDevice, height: 246 }} />
            );

        });
    }
    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }

    render() {
        var stranl = this.stran;
        var lang = this.state.Language;

        const item = this.state.item


        return (
            <View style={styles.container}>
                <View style={styles.container_top_profile}>
                    <Image source={{ url: item.img_url }} style={{ width: 50, height: 50, borderRadius: 10, marginLeft: 5, marginRight: 5, marginTop: 5 }}></Image>
                    <View>
                        <Text style={styles.namefood}>{eval("item.name_" + lang)}</Text>
                        {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.price}>{item.promotion_price} {item.currency}</Text> : null}
                        {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.pricepromotion}>{item.price} {item.currency}</Text> : null}
                        {!item.promotion_price || item.promotion_price < 1 ? <Text style={styles.price}>{item.price} {item.currency}</Text> : null}
                    </View>
                </View>

                <ScrollView>
                    <Swiper style={{ height: 200, marginBottom: 5 }} showsButtons={false} activeDotColor="white"
                        autoplay={true} loop={true} paginationStyle={styles.dot}
                        key={this.state.imgMoreCount.length}
                    >

                        {this.getImgMore(item.img_more)}
                    </Swiper>

                     <View style={styles.comboreview}>
                                    <View style={styles.comboitem} >
                                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Image source={require('../Images/hearticon.png')} style={{ width: 26, height: 26 }}></Image>
                                            <Text style={{ marginLeft: 3 }}>Love</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.comboitem} >
                                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Image source={require('../Images/commenticon.png')} style={{ width: 24, height: 24 }}></Image>
                                            <Text style={{ marginLeft: 3 }}>Comment</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.comboitem} >
                                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Image source={require('../Images/addcart.png')} style={{ width: 26, height: 26 }}></Image>
                                            <Text style={{ marginLeft: 3 }}>Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                    <View style={styles.content}>


                        <View style={styles.detailcontent}>



                            {/* <Text  style={styles.readmore}>Read more{"\n"}</Text> */}



                            <View style={{borderWidth:1,borderRadius:4,borderColor:"gray",margin:4,padding:4}}>
                                <Text style={styles.description}>{eval("item.description_" + lang)}</Text>
                            </View>
                            <Text style={styles.h2}>{stranl("Ingredient")}</Text>
                            <View>
                                <Text style={styles.description}>{eval("item.ingredient_" + lang)}</Text>
                            </View>
                            <Text style={styles.h2}>{stranl("Nutritional value")}</Text>
                            <View>
                                <Text style={styles.description}>{eval("item.nutritional_value_" + lang)}</Text>
                            </View>


                        </View>

                    </View>
                </ScrollView>



            </View>
        )
    }
}


const WDevice = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: 'white',
        marginTop: 0,
       


    },
    container_top_profile: {

        paddingTop: 10,
        flexDirection: "row",
        backgroundColor: "white"
    },
    h2: {
        marginTop: 5,
        color: "black",
        fontSize: 17
    },
    content: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-start",
        backgroundColor: "white",
        marginTop: 3,

        padding: 10,
        marginTop: 5,
    },
    detailcontent: {
        marginLeft: 3,

        backgroundColor: "#e0cdcd00",

    },
    namefood: {
        fontSize: 22,
        fontWeight: "bold",
        color: "#3F69AA"
    }, dot: {

        // marginLeft: 290,
        // marginBottom: 250

    }
    , price: {

        fontSize: 18,
        fontWeight: "bold",
        color: "#485167"

    }, pricepromotion: {
        marginTop: 1,
        fontSize: 15,
        fontWeight: "bold",
        color: "gray",
        textDecorationLine: "line-through",


    }, description: {
        color: "gray",
        fontSize: 17
    },
    readmore: {
        fontWeight: "bold"
    },
       comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center",
        backgroundColor:"white",
        marginTop:10,
        marginLeft:4,
         marginRight:4,
         borderRadius:20,
    },
    comboitem: {
         borderRadius:20,
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})





const mapStateToProps = state => {
    return {
        Lang: state.Language,

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem);