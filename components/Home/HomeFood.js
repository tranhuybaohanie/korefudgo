import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
  FlatList,
  Button,
  SearchBar,
  Icon,
  ScrollView
} from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import { navigate } from 'react-navigation'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import FoodList from '../../Mock/FoodList';
import FoodItem from './FoodItem'
import Header from '../Shared/Header';
import { GetAllFood } from '../../Networking/FetchApi'
import FoodListMock from '../../Mock/FoodList'
import SquareItem from './SquareItem'
import { StackNavigator, TabNavigator, ThemeProvider, uiTheme, Toolbar, NavigationActions } from 'react-navigation'
import { db } from '../../utlils/firebaseConfig';
import Slider from './Slider';
import SliderLoader from './SliderLoader';
import CategoryItem from './CategoryItem';
import CategoryItemLoader from './CategoryItemLoader';

import 'react';

import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';

const SIZE = 80;
class HomeFood extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Language: this.props.Lang[0]["lang"],
      title: "Food",
      isLoading: false,
      foodListFromServer: [],
      itemsMenu: [],
      itemsMenuComponent: [],
      itemsCategory: [],
      itemsCategoryComponent: [],
      itemsSlider: [],
      itemsSliderComponent: [],
      config_menu: {},
      sliderSession:0,

    };
  
    this._onPress = this._onPress.bind(this);
    this.growAnimated = new Animated.Value(0);
  }

  componentWillReceiveProps(newprops) {

    var previousLang = this.props.Lang[0]["lang"];
    var newLang = newprops.Lang[0]["lang"];
    if (this.state.Language != newLang) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      });

      const { navigation: { setParams } } = this.props;
      setParams({
        title: newprops.Lang[0]["lang"] == "en" ? "Food" : "Món ăn"
      });
    }
  }
  componentWillMount() {
    const { navigation: { setParams } } = this.props;
    setParams({
      title: this.state.Language == "en" ? "Food" : "Món ăn"
    });
  }
  componentDidMount() {
   

    actionRedux.getMenuConfig((resultConfig) => {
      this.setState({
        itemsMenu: [],
        itemsMenuComponent: [],
        itemsCategory: [],
        itemsCategoryComponent: [],
        itemsSlider: [],
        itemsSliderComponent: [],
        config_menu: resultConfig
      })
      resultConfig.itemsMenu.map((item, id) => {
        if (!this.state.itemsMenu.includes(item)) {
          actionRedux.getProductByID(item, (result) => {
            var product = this.state.itemsMenuComponent;
            var itemMenu = this.state.itemsMenu;
            var itemsCategory = this.state.itemsCategory;
            var itemsCategoryComponent = this.state.itemsCategoryComponent;
            var itemsSlider = this.state.itemsSlider;
            var itemsSliderComponent = this.state.itemsSliderComponent;
           
           
            if (result.bigCategoryName == "Food") {
              if (resultConfig.slider.includes(result.id)) {
                if (itemsSlider.includes(result.id)){
                  itemsSliderComponent[itemsSlider.indexOf(result.id)]=result;
                }else{
                itemsSlider.push(result.id)
                itemsSliderComponent.push(result)
                }
              } 
        
              if (itemMenu.includes(result.id)) {
                product[itemMenu.indexOf(result.id)] = result;
              } else {
              
                itemMenu.push(result.id)
              product.push(result)
              }
             
              // console.log(result)
              if (!itemsCategory.includes(result.product_category_id)) {
                itemsCategory.push(result.product_category_id)
                itemsCategoryComponent.push(result)

              }
              this.setState({
                itemsMenu: itemMenu,
                itemsMenuComponent: product,
                itemsCategory,
                itemsCategoryComponent,
                itemsSlider,
                itemsSliderComponent,
                sliderSession: this.state.sliderSession+1
              }, () => {
              
              })
            }
          })
        }
      })
    })


  }
  refreshDataFromServer = () => {
    GetAllFood('hotel', 'GET', null).then((res) => {

    });

  }

  static navigationOptions = ({ navigation }) => {


    return {
      title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'find' : navigation.state.params.title,

      //  headerRight:<Button title="info"></Button>,
      header: { visible: true },
      //  headerTinColor:'red',
      //   activeTintColor:'blue',
      tabBarIcon: ({ focused }) => (

        <Image
          source={require('../Images/cart.png')}
          style={{
            width: 26, height: 26,
            tintColor: focused ? 'green' : 'gray',
          }}
        />

      )
    }
  }

  _onPress() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });

    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 300,
      easing: Easing.quad,
    }).start();

    setTimeout(() => {
      Actions.pop();
    }, 500);
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  onScroll = (event) => {
    var currentOffset = event.nativeEvent.contentOffset.y;
    var direction = currentOffset > this.offset ? 'down' : 'up';
    this.offset = currentOffset;
    if (direction == "down") {
      this.props.screenProps.hideMenu(true)
    } else {
      this.props.screenProps.hideMenu(false)
    }
  }
  render() {
   
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, SIZE],
    });
    var { config_menu } = this.state;
    const { navigate } = this.props.navigation;
    var lang = this.state.Language;
    var transl =this.stran;
    return (

      <ScrollView style={styles.container} onScroll={this.onScroll}>

        {/* <Button title="Pushdetail"
          onPress={() => { navigate('FoodDetail') }}
        ></Button> */}
        <View>
        {this.state.itemsSliderComponent.length==0?
         < SliderLoader/>:
          <Slider navigate={this.props.screenProps.navigate} sliderSession={this.state.sliderSession} itemsSliderComponent={this.state.itemsSliderComponent} itemsSlider={this.state.itemsSlider}></Slider>
        }
          </View>

        <View style={styles.compoItem}>
          <Text style={{ fontSize: 19, flex: 0.05, textAlign: 'center', marginTop: 20 }}>{eval("config_menu.title_food_" + lang) ? eval("config_menu.title_food_" + lang) : "Welcome to Korefud"}</Text>
          <Text style={{ textAlign: 'center', color: 'gray', marginBottom: 20 }}>{eval("config_menu.caption_food_" + lang) ? eval("config_menu.caption_food_" + lang) : "Eat all these if you want! Drink all if you can!"}</Text>

          <View style={styles.compoItem}>
            <Text style={styles.textTitle}>{transl("product category")} </Text>
           {this.state.itemsCategoryComponent.length==0?
          <CategoryItemLoader/>:
              <CategoryItem navigate={this.props.screenProps.navigate} sliderSession={this.state.sliderSession} itemsCategoryComponent={this.state.itemsCategoryComponent} itemsCategory={this.state.itemsCategory} itemsMenu={this.state.itemsMenu} ></CategoryItem>
           }
          </View>
        </View>

        
        <View style={styles.compoItem}>

          <Text style={styles.textTitle}>NEWEST RECIPES ></Text>
          <ScrollView horizontal={true} >
            <SquareItem navigate={this.props.screenProps.navigate}  sliderSession={this.state.sliderSession} itemsMenuComponent={this.state.itemsMenuComponent}></SquareItem>
           
          </ScrollView>
          <Text style={styles.textTitle}>TRENDING RECIPES ></Text>
          <ScrollView horizontal={true} >
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
            <SquareItem></SquareItem>
          </ScrollView>
        </View>


        <View style={styles.compoItem}>
          <Text style={styles.textTitle}>TODAY ></Text>
          <FoodItem navigate={this.props.screenProps.navigate} sliderSession={this.state.sliderSession} itemsMenuComponent={this.state.itemsMenuComponent}></FoodItem>
         
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,

    // backgroundColor: '#b5b7bd'
    //alignItems: 'flex-end',
    //justifyContent: 'flex-end',
  },
  textTitle: {
    marginLeft: 8,
    marginTop: 10,
    marginBottom: 7,
    fontSize: 17,
    fontWeight: 'bold'

  },
  compoItem: {
    backgroundColor: 'white',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 40

  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: 100,
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  circle: {
    height: SIZE,
    width: SIZE,
    marginTop: -SIZE,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});

const mapStateToProps = state => {
  return {
    Lang: state.Language,

  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    fetchAllBigCategory: (result) => {

      dispatch(actionRedux.actFetchBigCategory(result));

    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeFood);