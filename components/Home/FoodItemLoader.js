import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions
} from 'react-native';

import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;
class FoodItem extends Component {
constructor(props){
    super(props);
    this.state={
        Language:"en",
        itemMenu:{}
    }
}
componentDidMount(){
    // actionRedux.getProductByID(this.props.item.item, itemMenu=>{
    //     this.setState({itemMenu})
    // })
  
}
    componentWillReceiveProps(newprops) {

        this.setState({ itemMenu: this.props.item.item })
        var previousLang = this.props.Lang[0]["lang"];

        if (this.props != newprops) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

        }
    }
limitString =(string,num)=>{
    
if(typeof string=="string")
return string.length>num ? string.slice(0,num)+"...":string.slice(0,num);
}

    stran = (key) => {
        return stranl(this.state.Language, key);
    }

    render() {

        var lang=this.state.Language;
   
        var {  index } = this.props.item;
        var item =this.state.itemMenu
        // console.log(item)



        return (
            <View key={index} style={styles.container}>

                <View style={styles.content}>
                    <Image source={{ url: item.img_url }} style={{ flex: 1, height: 200 }}></Image>
                    <Text style={styles.detailcontentback}> </Text>
                    <View style={styles.detailcontent}>
                        <Text style={styles.namefood}>{eval("item.name_"+lang)}</Text>
                        
                        <Text style={styles.description}>{this.limitString(eval("item.description_" + lang),40)}</Text>
                        {/* <Text  style={styles.readmore}>Read more{"\n"}</Text> */}
                     
                          
                        {item.promotion_price && item.promotion_price > 0 ?     <Text style={styles.price}>{item.promotion_price} {item.currency}{"\n"}</Text>:null}
                        {item.promotion_price && item.promotion_price > 0 ?  <Text style={styles.pricepromotion}>{item.price} {item.currency}</Text>:null}
                        {!item.promotion_price || item.promotion_price <1 ? <Text style={styles.price}>{item.price} {item.currency}</Text> : null}

                        

                    </View>

                </View>


                <View style={styles.comboreview}>
                    <View style={styles.comboitem} > 
                    <TouchableOpacity>
                        <Image source={require('../Images/hearticon.png')} style={{ width: 26, height: 26 }}></Image>

                    </TouchableOpacity>
                    </View>
                    <View style={styles.comboitem} > 
                    <TouchableOpacity>
                        <Image source={require('../Images/commenticon.png')} style={{ width: 26, height: 26 }}></Image>

                    </TouchableOpacity>
                    </View>
                    <View style={styles.comboitem} > 
                    <TouchableOpacity>
                        <Image source={require('../Images/addcart.png')} style={{ width: 26, height: 26 }}></Image>

                    </TouchableOpacity>
                    </View>
                </View>

            </View>
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        //backgroundColor: 'white',

        marginTop: 5,

    },
    content: {
        flex: 1,
        flexDirection: "column",
      //  backgroundColor: 'none',

        marginTop: 3,
        height: 200
    },
    detailcontent: {
        marginLeft:3,
        position: 'absolute',
        left: 0,
        top: 100,
        backgroundColor: "#e0cdcd00",
        height: 105
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 100,
        width: Dimensions.get('window').width,
        height: 100,
        backgroundColor: "#79747487",
    },namefood:{
        fontSize:22,
        fontWeight:"bold",
        color:"white"
    },price:{
        marginTop:20,
        fontSize:18,
        fontWeight:"bold",
        color:"yellow",
        flex:1

    },pricepromotion:{
        marginBottom:5,
        fontSize:15,
        fontWeight:"bold",
        color:"gray",
        textDecorationLine:"line-through",
      

    },description:{
        color:"white"
    },
     readmore:{
        fontWeight:"bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center"
    },
    comboitem: {
        height: 40,
        flex: 0.33,
        marginTop:3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})





const mapStateToProps = state => {
    return {
        Lang: state.Language,
      
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem);