import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';
import CategoryPageLoader from "./CategoryPageLoader"
import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;
class FoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: "en",
            itemsMenuComponent: [],
            sliderSession: 0,
            item_id: this.props.navigation.getParam('item_id'),
            itemsMenuProps: this.props.navigation.getParam('itemsMenu'),
            product_category_name: this.props.navigation.getParam('product_category_name'),
            itemsMenu: []
        }
    }
    componentDidMount() {
        const { navigation: { setParams } } = this.props;
        setParams({
            title: this.state.product_category_name
        });
        this.state.itemsMenuProps.map((item, id) => {
            actionRedux.getProductByID(item, result => {
                if (result.product_category_id == this.state.item_id) {
                    var itemsMenuComponent = this.state.itemsMenuComponent;
                    var itemsMenu = this.state.itemsMenu;
                    if (itemsMenu.includes(result.id)) {
                        itemsMenuComponent[itemsMenu.indexOf(result.id)] = result
                    } else {
                        itemsMenuComponent.push(result)
                        itemsMenu.push(result.id)
                    }
                    this.setState({ itemsMenuComponent, sliderSession: this.state.sliderSession + 1 })
                }

            })
        })


    }
    static navigationOptions = ({ navigation }) => {


        return {
            title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'Food' : navigation.state.params.title,
            headerStyle: {
                backgroundColor: '#D5AE41',


            }, navigationOptions: {
                header: {
                    style: {
                        shadowOpacity: 0,
                        shadowOffset: {
                            height: 0,
                            width: 0
                        },
                        shadowRadius: 0,
                    }
                }
            }
        }
    }
    componentWillReceiveProps(newprops) {
        // if (newprops.itemsMenuComponent)
        // console.log(newprops)
        // this.setState({ itemsMenuComponent: newprops.itemsMenuComponent, sliderSession: newprops.sliderSession })
        var previousLang = this.props.Lang[0]["lang"];
        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

        }

    }
    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail = (item) => {
        this.props.navigation.navigate("FoodIDetail", { item })
    }
    render() {

        var lang = this.state.Language;


        // console.log(item)



        return (
            // <FlatList
            //     sliderSession={this.state.sliderSession}
            //     data={this.state.itemsMenuComponent}
            //     keyExtractor={(item, index) => item.key}
            //     renderItem={(item, index) => {

            <ScrollView >

                {this.state.itemsMenuComponent.length==0?<View><CategoryPageLoader/><CategoryPageLoader/><CategoryPageLoader/></View>:this.state.itemsMenuComponent.map((item, index) => {
                    return (
                        <TouchableWithoutFeedback onPress={() => { this.showDetail(item) }} >
                            <View key={index} style={styles.container}>

                                <View style={styles.content}>
                                    <Image source={{ url: item.img_url }} style={{ flex: 1, height: 200 }}></Image>
                                    <Text style={styles.detailcontentback}> </Text>
                                    <View style={styles.detailcontent}>
                                        <Text style={styles.namefood}>{eval("item.name_" + lang)}</Text>

                                        <Text style={styles.description}>{this.limitString(eval("item.description_" + lang), 40)}</Text>
                                        {/* <Text  style={styles.readmore}>Read more{"\n"}</Text> */}


                                        {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.price}>{item.promotion_price} {item.currency}{"\n"}</Text> : null}
                                        {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.pricepromotion}>{item.price} {item.currency}</Text> : null}
                                        {!item.promotion_price || item.promotion_price < 1 ? <Text style={styles.price}>{item.price} {item.currency}</Text> : null}



                                    </View>

                                </View>


                                <View style={styles.comboreview}>
                                    <View style={styles.comboitem} >
                                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Image source={require('../Images/hearticon.png')} style={{ width: 26, height: 26 }}></Image>
                                            <Text style={{ marginLeft: 3 }}>Love</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.comboitem} >
                                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Image source={require('../Images/commenticon.png')} style={{ width: 24, height: 24 }}></Image>
                                            <Text style={{ marginLeft: 3 }}>Comment</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.comboitem} >
                                        <TouchableOpacity style={{ flexDirection: "row", alignItems: "center" }}>
                                            <Image source={require('../Images/addcart.png')} style={{ width: 26, height: 26 }}></Image>
                                            <Text style={{ marginLeft: 3 }}>Cart</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>
                        </TouchableWithoutFeedback>
                    )
                })

                }

            </ScrollView>
            /* }}
        ></FlatList>
    ) */
        )
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        //backgroundColor: 'white',
 marginLeft: 4,
        marginRight: 4,
         borderRadius:20,
        marginTop: 5,

    },
    content: {
        flex: 1,
        flexDirection: "column",
        //  backgroundColor: 'none',

        marginTop: 3,
        height: 200
    },
    detailcontent: {
        marginLeft: 20,
        position: 'absolute',
        left: 0,
        top: 100,
        backgroundColor: "#e0cdcd00",
        height: 105
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 100,
        width: Dimensions.get('window').width,
        height: 100,
        backgroundColor: "#79747487",
    }, namefood: {
        fontSize: 22,
        fontWeight: "bold",
        color: "white"
    }, price: {
        marginTop: 20,
        fontSize: 18,
        fontWeight: "bold",
        color: "yellow",
        flex: 1

    }, pricepromotion: {
        marginBottom: 5,
        fontSize: 15,
        fontWeight: "bold",
        color: "gray",
        textDecorationLine: "line-through",


    }, description: {
        color: "white"
    },
    readmore: {
        fontWeight: "bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center",
        backgroundColor:"white",
        marginTop:10,
         borderRadius:20,
    },
    comboitem: {
         borderRadius:20,
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})





const mapStateToProps = state => {
    return {
        Lang: state.Language,

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FoodItem);