import React, { Component } from 'react';
import HomeFood from './HomeFood'
import HomeDrink from './HomeDrink'
import { StackNavigator, TabNavigator } from 'react-navigation'
import Header from '../Shared/Header'
import {
    TabBarBottom,
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Button,
    SearchBar,
    Dimensions,
    Icon
} from 'react-native';
 


const WDevice = Dimensions.get('window').width;
const SubMain = TabNavigator({


    HomeFood: {
        screen: HomeFood,
        // navigationOptions: {

        //     tabBarLabel: "Food",

        // }
    },

    HomeDrink: {
        screen: HomeDrink,
        // navigationOptions: {

        //     tabBarLabel: "Drink",

        // }
    },
    Combo: {
        screen: HomeFood,
        navigationOptions: {

            tabBarLabel: "Combo",

        }
    },

},
    {
        lazy:false,
        // tabBarPosition: 'Bottom',
        swipeEnabled: true,

        ...TabNavigator.Presets.AndroidTopTabs,


        tabBarOptions: {
            // scrollEnabled: true,
            showIcon: false,
            showLabel: true,
           
         
            
            activeTintColor: '#ff0000',
             activeBackgroundColor: 'rgb(29, 144, 175)',
            animationEnabled: true,
            labelStyle: {
                color: 'black',
                fontSize: 12,
                
                fontWeight: 'bold',
                width:Dimensions/3
            },

            tabStyle: {
                width: (WDevice/3)-5,

                //borderLeftWidth: 1,
                //borderRightWidth: 1,

            },

            style: {
                // width: 600,
                backgroundColor: 'white',
                // color:'black'
            },
        }
    })



    
// this main to fix header and contain submain
const Main = TabNavigator({


    MainHome: {
    screen:(props)=>{ return( <SubMain screenProps= {{navigate:props.screenProps.navigate,hideMenu:props.screenProps.hideMenu}}></SubMain>)},
        navigationOptions: {

            tabBarLabel: "Tab 1",

        }
    }
}, 
    {
        
        ...TabNavigator.Presets.AndroidTopTabs,

        tabBarComponent: (props, state) => {
           return( <View style={{zIndex:1000}}>
                {/* {...TabNavigator.Presets.AndroidTopTabs} */}
                <Header screenProps={{show_menu:props.screenProps.show_menu}}></Header>
               
            </View>)
    }
    
        
    })

class MainNavigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = { show_menu: true};

    }
    // componentDidMount() {
    //     this.onLoadCats().then((cats) => this.setState({ cats: cats }));
    // }
    hideMenu=(req)=>{
        this.setState({
            show_menu:!req
        })
    }
    render() {
        return (<Main screenProps={{ navigate: this.props.navigation.navigate, show_menu: this.state.show_menu ,hideMenu:this.hideMenu}} />
        )}
}

export default MainNavigation;

