import React, { Component } from 'react';
import {
  CheckBox,
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  TextInput,
  Dimensions,
  Easing,
  StatusBar,
  Modal,
  TouchableWithoutFeedback
} from 'react-native';
import ls from 'react-native-local-storage';
import { Actions, ActionConst } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;

class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Language: this.props.Lang[0]["lang"] ? this.props.Lang[0]["lang"] : "vn",
      isLoading: false,
      flag_left: new Animated.Value(-5),
      optionLang_left: new Animated.Value(-200),
      isModalVisible: false,
      showMenu: new Animated.Value(0),
      isshowMenuOpacity:new Animated.Value(1),
    };


    this._onPress = this._onPress.bind(this);
    this.growAnimated = new Animated.Value(0);
  }
  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  componentWillReceiveProps(newprops) {
   
   if(newprops.screenProps){
    if (this.state.showMenu._value == -40&& newprops.screenProps.show_menu ){
     
      Animated.timing(
        this.state.showMenu,
        {
          toValue: 0,
          duration: 500
        }
      ).start();
     Animated.timing(
        this.state.isshowMenuOpacity,
        {
          toValue: 1,
          duration: 500
        }
      ).start();
    } else if (this.state.showMenu._value == 0 && !newprops.screenProps.show_menu ){
      Animated.timing(
        this.state.showMenu,
        {
          toValue: -40,
          duration: 500
        }
      ).start();
       Animated.timing(
        this.state.isshowMenuOpacity,
        {
          toValue: 0,
          duration: 500
        }
      ).start();
    }
  }
    var previousLang = this.props.Lang[0]["lang"];
    var newLang = newprops.Lang[0]["lang"];

    if (this.state.Language != newLang) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      });

    }
  }
  componentDidMount() {
    Animated.timing(
      this.state.flag_left,
      {
        toValue: 20,
        duration: 3000
      }
    ).start();
  }
  //   static navigationOptions =({navigation})=>{
  //     let tabBarLabel='Cart';
  //     let tabBarIcon=()=>{
  //       <Image source={require('../Images/left-arrow.png')} style={{width:26, height:26,tintColor:'white'}}/>
  //     }
  //     return {tabBarLabel,tabBarIcon}
  //   }
  static navigationOptions = {
    activeTintColor: '#81B247',
    tabBarIcon: () => (

      <Image
        source={require('../Images/bell.png')}
        style={{
          width: 26, height: 26,
          tintColor: focused ? 'green' : 'gray',
        }}
      />
    )
  }
  _onPress() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });

    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 300,
      easing: Easing.quad,
    }).start();

    setTimeout(() => {
      Actions.pop();
    }, 500);
  }
  hideOptionLang = () => {
    this.setState({ isModalVisible: false })
    Animated.timing(
      this.state.optionLang_left,
      {
        toValue: -200,
        duration: 1000
      }
    ).start();
  }
  showOptionLanguage = () => {
  
    this.setState({ isModalVisible: true })
    Animated.timing(
      this.state.optionLang_left
      , {
        toValue: 20,
        duration: 300,
        easing: Easing.quad,
      }).start();

  }
  setLang(lang) {
    var obLang = [{ lang: lang }]
    ls.save('Lang', lang).then(() => { })

    this.props.updateLang(obLang);
    this.setState({ isModalVisible: false })
    Animated.timing(
      this.state.optionLang_left,
      {
        toValue: -200,
        duration: 1000
      }
    ).start();
  }
  render() {
    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, SIZE],
    });

    return (
    
   <Animated.View style={{...styles.container,marginTop:this.state.showMenu}}>
        <LinearGradient
          colors={['#BD3D3A', '#E94B3C', 'orange']}
          start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
          style={{ height: 62, width: widthDevice,position:"absolute"}}
        ></LinearGradient>
        
       
        <StatusBar barStyle='light-content'></StatusBar>
        <TouchableOpacity activeOpacity={0.3} onPress={this.showOptionLanguage}>
          <Animated.Image source={this.state.Language == "vi" ? require("./../Images/flag-vn.png") : require("./../Images/flag-usa.png")} style={{ width: 27, height: 27, marginLeft: this.state.flag_left, marginTop: 15,opacity:this.state.isshowMenuOpacity }} />
        </TouchableOpacity>

        <Animated.View style={{...styles.boxsearch,opacity:this.state.isshowMenuOpacity}}>

          <Image source={require('./../Images/searchicon.png')} style={styles.searchicon}></Image>


          
          <TextInput
            style={styles.textinput}
            placeholder="Type here to search!"
            onChangeText={(text) => this.setState({ text })}
          />
        </Animated.View>
       
        <TouchableWithoutFeedback onPress={this.hideOptionLang} >
          <View style={{ flex: 1, top: 100, position: "absolute", height: heightDevice, width: widthDevice, display: this.state.isModalVisible ? "flex" : "none" }}>
          </View>
        </TouchableWithoutFeedback>

        <Animated.View style={{ ...styles.optionLanguage, marginLeft: this.state.optionLang_left }}>
          <LinearGradient
            colors={['#BD3D3A', '#E94B3C', 'orange']}
            start={{ x: 0.0, y: 0.0 }} end={{ x: 1.0, y: 1.0 }}
            style={{ width: 190, height: 100, position: "absolute" }}
          ></LinearGradient>
          <View>
          
        
            <TouchableOpacity onPress={() => { this.setLang("vi") }} >
              <View style={styles.textOptionLanguage}><Image style={{ width: 25, height: 25, marginRight: 10 }} source={this.state.Language == "vi" ? require("./../Images/icon-select.png") : null}></Image><Text style={{ fontSize: 20, color: "white" }}>Việt Nam</Text></View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => { this.setLang("en") }}>
              <View style={styles.textOptionLanguage}><Image style={{ width: 25, height: 25, marginRight: 10 }} source={this.state.Language == "en" ? require("./../Images/icon-select.png") : null}></Image><Text style={{ fontSize: 20, color: "white" }}>English</Text></View>
            </TouchableOpacity>
          </View>
        </Animated.View>
         
      </Animated.View>
     


    );
  }
}
const widthDevice = Dimensions.get('window').width;
const heightDevice = Dimensions.get('window').height;
const styles = StyleSheet.create({

  container: {
    height: 64,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'flex-start',
    // backgroundColor: '#009B77'
    // #45B8AC'
  },
  textOptionLanguage: {
    marginTop: 10,
    flexDirection: "row",
    fontWeight: "bold",
    color: "white"
  },
  textinput: {
    color: 'white'
  }, 
  optionLanguage: {
    position: 'absolute',
    width: 190,
    height: 100,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",

    top: 70,
    zIndex: 1000,
    borderRadius: 12,
    // backgroundColor: '#009b77e0',
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: 'black',
    shadowOffset: { height: 0, width: 0 },
  },

  boxsearch: {
    left: 65,
    width: 290,
    backgroundColor: 'white',
    marginBottom: 3,
    borderColor: 'black',
    borderRadius: 50,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 5
  },
  searchicon: {
    width: 30,
    height: 30,


  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: 100,
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  circle: {
    height: SIZE,
    width: SIZE,
    marginTop: -SIZE,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});
const mapStateToProps = state => {
  return {
    Lang: state.Language,

  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    updateLang: (Lang) => {

      dispatch(actionRedux.updateLanguage(Lang))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);