import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    ImageBackground,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions
} from 'react-native';

import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index'
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import Swiper from 'react-native-swiper';

const SIZE = 80;


 export default class SliderLoader extends Component {

    constructor(props) {
        super(props);
        this.state = {
         backgroundColor: new Animated.Value(0)
        }
        
    }
  
    componentDidMount(){
setInterval(()=>{
    var value=this.state.backgroundColor._value==0?1:0;
Animated.timing(this.state.backgroundColor,
        {
            toValue:value,
            duration:1900,
             easing: Easing.quad,
    // useNativeDriver: true
        }).start()
},2000)
        
    }
    render() {
var backgroundColor = this.state.backgroundColor.interpolate({
    inputRange: [0, 1],
    outputRange: ['#50525229', 'gray']
});
        return (

            <Swiper style={styles.wrapper} showsButtons={false} activeDotColor="white"
                autoplay={true} loop={true} paginationStyle={styles.dot}
                // key={this.state.itemSliderComponent.length}
            >
                 <ImageBackground   blurRadius={0} style={{ ...styles.containt }} >

                    <Animated.View style={{...styles.contentSlider,backgroundColor:backgroundColor}}>
                        <View style={{height:50,borderRadius:10,marginLeft:10}}>
                            <Image style={{ ...styles.slide1, width: 50, height: 58 ,borderRadius:10}} ></Image>
                        </View>
                        <View style={styles.comboText}>
                             <Animated.Text style={{backgroundColor:backgroundColor,height: 10, width: 200 }}></Animated.Text>
                      <Animated.Text style={{backgroundColor:backgroundColor,height: 20,marginTop:10, width: 220, }}></Animated.Text>
                        </View>
                    </Animated.View>
                   
                </ImageBackground>
            </Swiper>
        );
    }
}



const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
    dot: {

        marginLeft: 290,
        marginBottom: 150

    },
    wrapper: {

        height: 200
    },
    contentSlider: {
        // flex: 1,
        height:70,
        width: WDevice,
   
        // backgroundColor:"black",
        backgroundColor:"#50525229",
        alignItems: "center",
        flexDirection: 'row'

    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#9DD6EB',

    },
    containt: {
        flex: 1,
        height:200,
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    comboText:{
        marginLeft:5
    },  
      text: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold',
    },
    textDescription: {
        color: '#fff',
        fontSize: 13,
        
    }
})


