import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';

import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import ls from 'react-native-local-storage'
import {sound} from './../../utlils/sound'
import Swipeout from 'react-native-swipeout'
const SIZE = 80;
class CartFoodItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: this.props.Lang[0]["lang"],
            sliderSession: 0,
            CartComponent: {},
            item: this.props.item.item,
            quantity: this.props.item.item.item,
            Cart: this.props.Cart[0].itemList,
            Deleled:false
        }
      
       
    }
    btnSetQuantity=(sign)=>{
        var Cart = this.state.Cart;
        var {item}=this.state;
        if (Cart.some(e => e.id ===item.id )) { 
            sound("click.mp3",0.1)
            var math = sign == "+" ? +1 : Cart[Cart.findIndex(i => i.id === item.id)].quantity >1? -1:0;
            Cart[Cart.findIndex(i => i.id === item.id)].quantity = Cart[Cart.findIndex(i => i.id === item.id)].quantity+ math;
            ls.save('Cart', Cart).then(() => { })
            this.props.UpdateCart([{
                itemList: Cart
            }])
          
        }
    }
    componentDidMount() {
      
            actionRedux.getProductByID(this.state.item.id, (result) => {
                this.setState({
                    CartComponent: result,
                    sliderSession: this.state.sliderSession + 1
                }, () => {})

            })

    }
    componentWillReceiveProps(newprops) {
       if(newprops.Cart[0].itemList){
           var { item } = this.state;
           this.setState({
               Cart: newprops.Cart[0].itemList,
               quantity: newprops.Cart[0].itemList[newprops.Cart[0].itemList.findIndex(i => i.id === item.id)] ? newprops.Cart[0].itemList[newprops.Cart[0].itemList.findIndex(i => i.id === item.id)].quantity:0
           })
       }
            this.setState({ item: newprops.item.item })
         
                actionRedux.getProductByID(item.id, (result) => {
                    this.setState({
                        CartComponent: result,
                        sliderSession: this.state.sliderSession + 1
                    })

                })

        
        
        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

        }

    }
    deleteCart = () => {

        var Cart = this.state.Cart;
        var { item } = this.state;
        // alert(item.id)
        if (Cart.some(e => e.id === item.id)) {
            Cart = Cart.filter(i => i.id != item.id)
            sound("click.mp3", 0.1)
            ls.save('Cart', Cart).then(() => { })
            this.props.UpdateCart([{
                itemList: Cart
            }])
            // this.setState({ Deleled: true })
            // this.props.refresh(this.props.index)
        }
    }
  
    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail = (item) => {
        // this.props.navigate("FoodIDetail", { item })
    }
    render() {

        var lang = this.state.Language;
var stranl=this.stran;

        // console.log(item)

var item=this.state.CartComponent

        var swipeoutBtns = typeof this.props.pagefor =="string"&& this.props.pagefor === "checkout" ?[]: [
            {
                onPress:() => {
                this.deleteCart()
                },
                text:"Delete", 
                type:"delete"
             
            }
        ];
        return (
            <View key={this.props.index} style={{...styles.bigcontainer,display:this.state.Deleled?"none":"flex"}}>
                <Swipeout style={{ ...styles.bigcontainer }} autoClose={true} onClose={(secid, rowid, direction) => {}} onOpen={(secid, rowid, direction) => { 
                 
         }}
                    right={swipeoutBtns}
            sectionId={1}
             rowId={this.props.index}
 
           >
             
                    <TouchableWithoutFeedback key={this.props.index}
                        onPress={() => {  this.props.deleteCart(this.state.item.id)}}
                        // onPress={() => { this.showDetail(item) }} 
                        >
                            <View key={1} style={styles.container}>

                                <View style={styles.content}>
                                    <View style={{flexDirection:"row"}}>
                                    <Image source={{ url: item.img_url }} style={{ height: 70,width:70}}></Image>
                                     <View style={styles.detailcontent}>
                                        <Text style={styles.namefood}>{this.limitString(eval("item.name_" + lang),35)}</Text>
                                       
                                        {item.promotion_price && item.promotion_price > 0 ? <Text style={styles.price}>{item.promotion_price} {item.currency}</Text> : null}
                                        {item.promotion_price && item.promotion_price > 0 ?
                                             <View style={{flexDirection:"row"}}><Text style={styles.pricepromotion}>{item.price} {item.currency}</Text> 
                                        <Text> -{Math.round(100-(item.promotion_price/item.price)*100)}%</Text>
                                        </View>: null}
                                        {!item.promotion_price || item.promotion_price < 1 ? <Text style={styles.price}>{item.price} {item.currency}</Text> : null}
                                                                            </View>
                                   </View>
                                    {/* <Text style={styles.detailcontentback}> </Text> */}
                                  
                                       {typeof this.props.pagefor =="string"&& this.props.pagefor === "checkout" ? 
                                    <View><Text>{stranl("Quantity: ") +this.state.quantity }</Text></View>
                                    :<View style={styles.quantityBox}>
                                <TouchableOpacity onPress={() =>this.btnSetQuantity("+")}>
                                    <Text style={{ textAlign: "center"}}>+</Text>      
                                         </TouchableOpacity>
                                <Text style={{
                                    textAlign: "center",
                                    borderWidth:0.5,
                                    borderColor: "black"}}>{this.state.quantity}</Text>
                                <TouchableOpacity onPress={() =>this.btnSetQuantity("-")}>
                                    <Text style={{ textAlign:"center"}}>-</Text>      
                                         </TouchableOpacity>
                                        {/* <Text  style={styles.readmore}>Read more{"\n"}</Text> */}
                                        
                                    </View>}
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    {/* )
                })

                } */}

          
                </Swipeout>
            </View>
          
        )
    }
}



const styles = StyleSheet.create({
    bigcontainer:{
        backgroundColor:"white",
        marginTop:3,
        padding:7
    },
    container: {
        flex: 1,
        flexDirection: "column",
        //backgroundColor: 'white',

        marginTop: 3,

    },
    content: {
        flex: 1,
        flexDirection: "row",
        justifyContent:"space-between",
        //  backgroundColor: 'none',

        // marginTop: 2,
        height: 74
    },
    detailcontent: {
        marginLeft: 3,
        backgroundColor: "#e0cdcd00",
        height: 105
    }, 
    quantityBox: {
        minWidth:20,
      flexDirection:"column",
      alignContent:"center",
      justifyContent:"space-between",
        marginLeft: 3,
        borderRadius: 3,
        borderColor: "black",
        borderWidth: 1,
        backgroundColor: "#e0cdcd00",
        height: 57
    }, 
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 100,
        width: Dimensions.get('window').width,
        height: 100,
        backgroundColor: "#79747487",
    }, namefood: {
        fontSize: 15,
      
        color: "black"
    }, price: {
        fontWeight:"100",
        marginTop: 5,
        fontSize: 13,
        color: "red",
   

    }, pricepromotion: {
       fontWeight:"100",
        fontSize: 11,
       
        color: "gray",
        textDecorationLine: "line-through",


    }, description: {
        color: "white"
    },
    readmore: {
        fontWeight: "bold"
    },
       comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center",
        backgroundColor:"white",
        marginTop:10,
         borderRadius:20,
    },
    comboitem: {
         borderRadius:20,
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})






const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
    
        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CartFoodItem);