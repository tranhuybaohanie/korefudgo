import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
  FlatList,
  Button,
  SearchBar,
  Icon,
  ScrollView,
  Dimensions,
  Alert,
  ImageBackground
} from 'react-native';
import Svg, {
  Circle,
  Ellipse,
  G,
  LinearGradient,
  RadialGradient,
  Line,
  Path,
  Polygon,
  Polyline,
  Rect,
  Symbol,
  Text as TextSvg,
  Use,
  Defs,
  Stop
} from 'react-native-svg';
import { Actions, ActionConst } from 'react-native-router-flux';
import { navigate } from 'react-navigation'
import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import FoodList from '../../Mock/FoodList';

import Header from '../Shared/Header';
import { GetAllFood } from '../../Networking/FetchApi'
import FoodListMock from '../../Mock/FoodList'
import SquareItem from './SquareItem'
import { StackNavigator, TabNavigator, ThemeProvider, uiTheme, Toolbar, NavigationActions } from 'react-navigation'
import { db } from '../../utlils/firebaseConfig';
import Slider from './Slider';
import SliderLoader from './SliderLoader';
import CategoryItem from './CategoryItem';
import CategoryItemLoader from './CategoryItemLoader';

import 'react';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import ls from 'react-native-local-storage'
import { sound } from './../../utlils/sound'
import { connect } from 'react-redux';
import CartFoodItem from './CartFoodItem'

const SIZE = 80;
class Cart extends Component {

  constructor(props) {
    super(props);

    this.state = {
      Language: this.props.Lang[0]["lang"],
      title: "Food",
      Cart: this.props.Cart[0].itemList,
      sliderSession: 0,
      CartComponent: [],
      totalItem: 0,
      totalMoney: 0,
      savingMoney: 0,
      refresh: false,
      newpropsRunning: false,
    };

    this._onPress = this._onPress.bind(this);
    this.growAnimated = new Animated.Value(0);
  }

  componentWillReceiveProps(newprops) {

    // if (this.state.Cart.length != newprops.Cart[0].itemList.length) {

    this.setState({ Cart: newprops.Cart[0].itemList, sliderSession: this.state.sliderSession + 1 }, () => { console.log(this.state.Cart) })
    let totalItem = 0;
    let totalMoney = 0;
    let savingMoney = 0;
    this.setState({  newpropsRunning: true })
    newprops.Cart[0].itemList.map((item, id) => {
      totalItem += item.quantity;
      this.setState({ totalItem})
      actionRedux.getProductByID(item.id, (result) => {
        totalMoney += parseFloat(result.promotion_price > 0 ? result.promotion_price : result.price) * item.quantity;
        savingMoney += parseFloat(result.promotion_price > 0 ? result.price - result.promotion_price : 0) * item.quantity;

        var CartComponent = this.state.CartComponent;


        CartComponent.push(result)
        this.setState({
          totalMoney,
          savingMoney,
          CartComponent: CartComponent,
          sliderSession: this.state.sliderSession + 1
        }, () => { })

      })
    })
    // }

    var newLang = newprops.Lang[0]["lang"];
    if (this.state.Language != newLang) {
      this.setState({
        Language: newprops.Lang[0]["lang"],
      });

      const { navigation: { setParams } } = this.props;
      setParams({
        title: newprops.Lang[0]["lang"] == "en" ? "Cart" : "Giỏ hàng"
      });
    }
  }
  componentWillMount() {
    const { navigation: { setParams } } = this.props;
    setParams({
      title: this.props.Lang[0]["lang"] == "en" ? "Cart" : "Giỏ hàng"
    });
  }
  componentDidMount() {
    let totalItem = 0;
    let totalMoney = 0;
    let savingMoney = 0;
    this.state.Cart.map((item, id) => {
      totalItem += item.quantity;
      this.setState({ totalItem })
      actionRedux.getProductByID(item.id, (result) => {
        if (!this.state.newpropsRunning) {
          totalMoney += parseFloat(result.promotion_price > 0 ? result.promotion_price : result.price) * item.quantity;
          savingMoney += parseFloat(result.promotion_price > 0 ? result.price - result.promotion_price : 0) * item.quantity;

          var CartComponent = this.state.CartComponent;


          CartComponent.push(result)
          this.setState({
            totalMoney,
            savingMoney,
            CartComponent: CartComponent,
            sliderSession: this.state.sliderSession + 1
          }, () => { })
        }
      })
    })

  }
  deleteCart = (id) => {

    var Cart = this.state.Cart;
    if (Cart.some(e => e.id === id)) {
      // Cart = Cart.filter(i => i.id != id)
      Cart = Cart.slice(i => i.id == id)
      this.setState({ Cart })
      sound("click.mp3", 0.1)
      ls.save('Cart', Cart).then(() => { })
      this.props.UpdateCart([{
        itemList: Cart
      }])

    }
  }


  static navigationOptions = ({ navigation }) => {


    return {
      title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'find' : navigation.state.params.title,

      //  headerRight:<Button title="info"></Button>,
      header: { visible: true },
      swipeEnabled: false,
      //  headerTinColor:'red',
      //   activeTintColor:'blue',
      tabBarIcon: ({ focused }) => (

        <Image
          source={require('../Images/cart.png')}
          style={{
            width: 26, height: 26,
            tintColor: focused ? 'green' : 'gray',
          }}
        />

      )
    }
  }

  _onPress() {
    if (this.state.isLoading) return;

    this.setState({ isLoading: true });

    Animated.timing(this.growAnimated, {
      toValue: 1,
      duration: 300,
      easing: Easing.quad,
    }).start();

    setTimeout(() => {
      Actions.pop();
    }, 500);
  }

  stran = (key) => {
    return stranl(this.state.Language, key);
  }
  onScroll = (event) => {
    // var currentOffset = event.nativeEvent.contentOffset.y;
    // var direction = currentOffset > this.offset ? 'down' : 'up';
    // this.offset = currentOffset;
    // if (direction == "down") {
    //   this.props.screenProps.hideMenu(true)
    // } else {
    //   this.props.screenProps.hideMenu(false)
    // }
  }
  navigateCheckOut=()=>{
    if(this.state.Cart.length==0){
      Alert.alert(
       this.stran('Block Check Out'),
        this.stran('Go placing items to cart to check out.'),
        [
        
          { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'OK', onPress: () => this.props.screenProps.navigate("Home") },
        ],
        { cancelable: false }
      )
    }else{
      this.props.screenProps.navigate("CheckOut")
    }
   
  }
  refresh = () => {

    // this.setState({ refresh:true })
  }
  render() {

    const changeScale = this.growAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: [1, SIZE],
    });
    var { config_menu } = this.state;
    const { navigate } = this.props.navigation;
    var lang = this.state.Language;
    var transl = this.stran;
    return (
      <View style={styles.container} >
        <ScrollView onScroll={this.onScroll}>
        {this.state.Cart.length==0?<View>
            <ImageBackground source={require("./../Images/cart-empty-banner.jpg")} style={{ width:WDevice,height:300,marginTop:10,justifyContent:"center",alignItems:"center"}}>
              <TouchableOpacity onPress={() => this.props.screenProps.navigate("Home")}>
                <Svg
                  height="180"
                  width="180"
                >
                  <Text style={{ fontSize: 18, color: 'white', textAlign: "center", paddingTop: 60 }}>{transl("Go placing your dishes to cart now ")}</Text>
                  <Defs>
                    <LinearGradient id="grad" x1="0" y1="0" x2="180" y2="0">
                      <Stop offset="0" stopColor="orange" stopOpacity="0.8" />
                      <Stop offset="1" stopColor="#E94B3C" stopOpacity="1" />
                    </LinearGradient>
                  </Defs>

                  <Polygon
                    points="30,0 150,0 180,30 180,150 150,180 30,180 0,150 0,30 "
                    fill="url(#grad)"

                  />

                </Svg>

          </TouchableOpacity>
          </ImageBackground>
        </View>:null}
          <FlatList
            data={this.state.Cart}
            extraData={this.props}
            // refreshing={this.state.refresh}
            // onRefresh={this.refresh}
            keyExtractor={item => item.id}
            renderItem={(item, rowID) => <CartFoodItem item={item} index={rowID} deleteCart={this.deleteCart} />}
          />
          {/* {
          this.state.Cart.map((item,id)=>{
          
            return(
              <CartFoodItem item={item} index={id} deleteCart={this.deleteCart}/>
            )
          })
        } */}



        </ScrollView>

        <View style={styles.btnOrder}>
          <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
            <View style={{ marginLeft: 15, backgroundColor: "red", width: 10, height: 10 }}></View>
            <View style={{ marginTop: 12 }}><Text style={{}}>  {transl("Total: ") + this.state.totalItem + " items, " + this.state.totalMoney + " VND"}</Text>
              <Text style={{ fontSize: 9, color: "gray" }}>  {this.state.savingMoney > 0 ? transl("You are saving ") + this.state.savingMoney + " VND" : transl("Are you ready to eat")}</Text>
            </View>
          </View>
          <TouchableOpacity onPress={this.navigateCheckOut}>
          <View >

            <Svg
              height="50"
              width="180"
            >
              <Text style={{ fontSize: 18, color: 'white', textAlign: "center", paddingTop: 14 }}>{transl("Check Out")}</Text>
              <Defs>
                <LinearGradient id="grad" x1="0" y1="0" x2="180" y2="0">
                  <Stop offset="0" stopColor="orange" stopOpacity="0.8" />
                  <Stop offset="1" stopColor="#E94B3C" stopOpacity="1" />
                </LinearGradient>
              </Defs>

              <Polygon
                points="50,0 180,0 180,50 0,50 "
                fill="url(#grad)"

              />

            </Svg>

          </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const WDevice = Dimensions.get('window').width;
const styles = StyleSheet.create({
  container: {
    flex: 1,

    // backgroundColor: '#b5b7bd'
    //alignItems: 'flex-end',
    // justifyContent: 'space-between',
  }, btnOrder: {
    backgroundColor: "white",
    width: WDevice,
    height: 50,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  textTitle: {
    marginLeft: 8,
    marginTop: 10,
    marginBottom: 7,
    fontSize: 17,
    fontWeight: 'bold'

  },
  compoItem: {
    backgroundColor: 'white',
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 40

  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: SIZE,
    height: SIZE,
    borderRadius: 100,
    zIndex: 99,
    backgroundColor: '#F035E0',
  },
  circle: {
    height: SIZE,
    width: SIZE,
    marginTop: -SIZE,
    borderRadius: 100,
    backgroundColor: '#F035E0',
  },
  image: {
    width: 24,
    height: 24,
  },
});


const mapStateToProps = state => {
  return {
    Lang: state.Language,
    Cart: state.Cart,
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {

    UpdateCart: (result) => {

      dispatch(actionRedux.actUpdateCart(result));

    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Cart);