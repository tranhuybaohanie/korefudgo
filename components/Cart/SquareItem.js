import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    ScrollView,
    TouchableWithoutFeedback
} from 'react-native';

import { Actions, ActionConst } from 'react-native-router-flux';

import arrowImg from '../Images/left-arrow.png';
import { black } from 'ansi-colors';
import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
const SIZE = 80;
class Squareitem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Language: "en",
            itemsMenuComponent: []
        }
    }
    componentDidMount() {
        // actionRedux.getProductByID(this.props.item.item, itemMenu=>{
        //     this.setState({itemMenu})
        // })

    }
    componentWillReceiveProps(newprops) {

        function dynamicSort(property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }
        if (newprops.itemsMenuComponent){
            var itemsMenuComponent = [...newprops.itemsMenuComponent];
            this.setState({ itemsMenuComponent: itemsMenuComponent.sort(dynamicSort("-create_date")).slice(0, 2) })
        }
       
        var previousLang = this.props.Lang[0]["lang"];
        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });



        }
    }
    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }

    stran = (key) => {
        return stranl(this.state.Language, key);
    }
    showDetail = (item) => {
        this.props.navigate("FoodIDetail", { item })
    }
    render() {

        var lang = this.state.Language;

        var item = this.state.itemMenu
        // console.log(item)itemsMenuComponent




        return (
            <ScrollView horizontal={true} >
            {this.state.itemsMenuComponent.map((item,id)=>{

                    return(
                        <TouchableWithoutFeedback onPress={() => { this.showDetail(item) }} >
                        <View style={styles.container} key={id}>
                            <Image style={{ flex: 1, height: 100, borderRadius: 10 }} source={{ url: 'https://cdn.cnn.com/cnnnext/dam/assets/171027052520-processed-foods-exlarge-tease.jpg' }} />
                            {/* <View style={styles.detailcontentback}></View>
                <Text style={styles.detailcontent}>TRENDING RECIPES</Text>
                <Text style={styles.price}>25000</Text>
                <Text style={styles.pricepromotion}>20000</Text> */}
                            <View >
                                <Text style={styles.detailcontent}>{eval("item.name_"+lang)}</Text>
                                <Text style={styles.price}>{item.promotion_price.length > 0 ?item.promotion_price:item.price}</Text>
                              {item.promotion_price.length > 0?  <Text style={styles.pricepromotion}>{item.price}</Text>:<Text></Text>}
                            </View>
                        </View>
                        </TouchableWithoutFeedback>
                    )

            })}
    
            </ScrollView>
          
        )

    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        //  height: 110, 
        width: 180,
        marginLeft: 4,
        marginRight: 4,
        borderRadius: 10
        //backgroundColor: 'white',



    },
    content: {
        flex: 1,
        flexDirection: "column",
        //  backgroundColor: 'none',

        marginTop: 3,
        height: 200
    },
    detailcontent: {
        marginLeft: 4,

        // backgroundColor: "#e0cdcd00",

        // color:'white',
    },
    detailcontentback: {
        position: 'absolute',
        left: 0,
        top: 60,
        width: Dimensions.get('window').width,
        height: 100,
        backgroundColor: "#79747487",
    }, namefood: {
        fontSize: 25,
        fontWeight: "bold",
        color: "white"
    }, price: {
        marginLeft: 3,

        fontWeight: "bold",



    }, pricepromotion: {
        marginLeft: 3,

        fontWeight: "bold",
        color: "gray",
        textDecorationLine: 'line-through'

    }, description: {
        color: "white"
    },
    readmore: {
        fontWeight: "bold"
    },
    comboreview: {
        flex: 1, flexDirection: 'row',
        justifyContent: "center"
    },
    comboitem: {
        height: 40,
        flex: 0.33,
        marginTop: 3,
        borderStyle: 'solid',
        borderTopColor: '#eaebed',
        borderTopWidth: 2,
        backgroundColor: 'white',
        justifyContent: "center",
        alignContent: "center",
        alignItems: 'center',

    },
    comboitemtext: {

        alignContent: "center",
        alignItems: 'center',
        fontWeight: 'bold'
    }
})


const mapStateToProps = state => {
    return {
        Lang: state.Language,

    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        fetchAllBigCategory: (result) => {

            dispatch(actionRedux.actFetchBigCategory(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Squareitem);