import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Animated,
    Easing,
    FlatList,
    Dimensions,
    ScrollView,
    TextInput
} from 'react-native';
import Svg, {
    Circle,
    Ellipse,
    G,
    LinearGradient,
    RadialGradient,
    Line,
    Path,
    Polygon,
    Polyline,
    Rect,
    Symbol,
    Text as TextSvg,
    Use,
    Defs,
    Stop
} from 'react-native-svg';

import * as actionRedux from './../../Storage/Actions/Index';
import { stranl } from './../../utlils/StranlatetionData';
import { connect } from 'react-redux';
import CartFoodItem from './CartFoodItem'
import { TextField } from 'react-native-material-textfield';
import ls from 'react-native-local-storage';
import AwesomeAlert from 'react-native-awesome-alerts';
// import Geocoder from 'react-native-geocoding';
// Geocoder.init('AIzaSyD1zSRhwykOgyFaYMecyHpsaW1R1BVgiiE'); 
const SIZE = 80;
class CheckOut extends Component {

    constructor(props) {
        super(props);

        this.state = {
            Language: this.props.Lang[0]["lang"],
            alert:{
                title:"",
                message:"",
                cancel_text:"",
                confirm_text:"",
                confirm_function:()=>{},
                cancel_function:()=>{},
                showCancel:true,
                showConfirm:true,
                showProgress:false,
                show:false,
            },
            title: "Food",
            Cart: this.props.Cart[0].itemList,
            sliderSession: 0,
            CartComponent: [],
            CartComponentPrice: [],
            totalItem: 0,
            totalMoney: 0,
            savingMoney: 0,
            refresh: false,
            newpropsRunning: false,
            showAddAdress: new Animated.Value(800),
            backshowAddAdress: new Animated.Value(0),
            fullname: "",
            phone: "",
            address: ""
            // location:[]
        };

        this.growAnimated = new Animated.Value(0);
    }
    btnPlaceOrder=()=>{
        this.showPropress()
        var state=this.state;
        if (this.state.fullname != "" && this.state.address != "" && this.state.phone != "" && this.state.Cart.length == this.state.CartComponent.length && typeof this.state.CartComponent == "object") {
            actionRedux.addOrder(state.fullname, state.phone, state.address, state.CartComponentPrice,state.Cart ,state.totalItem, state.totalMoney, state.savingMoney,result=>{
            if(result==false){
                this.showAlert("Warning", "Sorry, you cannot place order at this moment!!");
            }else{
                var carEmpty=[];
                ls.save('Cart', carEmpty).then(() => { })
                this.props.UpdateCart([{
                    itemList: carEmpty
                }])
                this.showAlert("Congratulation", " your order was placed!!",
                  ()=>  this.props.navigation.navigate("Home"),
                    ()=>this.props.navigation.navigate("Home"))
            }})
            
       }else{
           this.showAlert("Warning", "Please, update your address!!");
       }
    }
    componentWillReceiveProps(newprops) {

        // if (this.state.Cart.length != newprops.Cart[0].itemList.length) {

        this.setState({ Cart: newprops.Cart[0].itemList, sliderSession: this.state.sliderSession + 1 }, () => { })
        let totalItem = 0;
        let totalMoney = 0;
        let savingMoney = 0;
        this.setState({ newpropsRunning: true })
        newprops.Cart[0].itemList.map((item, id) => {
            totalItem += item.quantity;
            this.setState({ totalItem })
            actionRedux.getProductByID(item.id, (result) => {
                totalMoney += parseFloat(result.promotion_price > 0 ? result.promotion_price : result.price) * item.quantity;
                savingMoney += parseFloat(result.promotion_price > 0 ? result.price - result.promotion_price : 0) * item.quantity;
                var CartComponent = this.state.CartComponent;
                var CartComponentPrice = this.state.CartComponentPrice;
                CartComponent.push(result)
                CartComponentPrice.push({
                    id:result.id,
                    price:result.promotion_price>0?result.promotion_price:result.price})
                this.setState({
                    totalMoney,
                    savingMoney,
                    CartComponent: CartComponent,
                    CartComponentPrice,
                    sliderSession: this.state.sliderSession + 1
                }, () => { })

            })
        })
        // }

        var newLang = newprops.Lang[0]["lang"];
        if (this.state.Language != newLang) {
            this.setState({
                Language: newprops.Lang[0]["lang"],
            });

            const { navigation: { setParams } } = this.props;
            setParams({
                title: newprops.Lang[0]["lang"] == "en" ? "Cart" : "Giỏ hàng"
            });
        }
    }
    componentWillMount() {
        const { navigation: { setParams } } = this.props;
        setParams({
            title: this.props.Lang[0]["lang"] == "en" ? "Cart" : "Giỏ hàng"
        });
    }
    componentDidMount() {
        let totalItem = 0;
        let totalMoney = 0;
        let savingMoney = 0;
        this.state.Cart.map((item, id) => {
            totalItem += item.quantity;
            this.setState({ totalItem })
            actionRedux.getProductByID(item.id, (result) => {
                if (!this.state.newpropsRunning) {
                    totalMoney += parseFloat(result.promotion_price > 0 ? result.promotion_price : result.price) * item.quantity;
                    savingMoney += parseFloat(result.promotion_price > 0 ? result.price - result.promotion_price : 0) * item.quantity;

                    var CartComponent = this.state.CartComponent;
                    var CartComponentPrice = this.state.CartComponentPrice;
                    CartComponent.push(result)
                    CartComponentPrice.push({
                        id: result.id,
                        price: result.promotion_price > 0 ? result.promotion_price : result.price
                    })

                    this.setState({
                        totalMoney,
                        savingMoney,
                        CartComponent: CartComponent,
                        CartComponentPrice,
                        sliderSession: this.state.sliderSession + 1
                    }, () => { })
                }
            })
        })
        ls.get('email').then((email) => {

            if (email.trim().length > 0) {
                actionRedux.getAllUserCustomerCondition(email, (id, result) => {

                    if (result != false) {
                        if(typeof result!="undefined"){
                        this.setState({
                            fullname: result.address.fullname ? result.address.fullname : "",
                           phone: result.address.phone ? result.address.phone:"",
                           address: result.address.address ? result.address.address : "",
                    })
                    
                    }}
                })


            } else {
              
            }

        }).catch((e) => {
            alert("login fail", e)
        });
    }
    static navigationOptions = ({ navigation }) => {


        return {
            title: typeof (navigation.state.params) === 'undefined' || typeof (navigation.state.params.title) === 'undefined' ? 'Food' : navigation.state.params.title,
            headerStyle: {
                backgroundColor: 'white',


            }, navigationOptions: {
                header: {
                    style: {
                        shadowOpacity: 0,
                        shadowOffset: {
                            height: 0,
                            width: 0
                        },
                        shadowRadius: 0,
                    }
                }
            }
        }
    }
    btnAddAddress = () => {
        if (this.state.fullname != "" && this.state.address != "" && this.state.phone != "") {
            var { fullname, phone,address} =this.state;
           
            actionRedux.updateAddAddress({ fullname, phone, address}, result => {
                this.hideAddNewAdress()
            })
        }else{
            this.showAlert("Warning","Fill all input, please!!")
        }

    }
    showAlert = (title, message,confirm_function,cancel_function,cancel_text,confirm_text)=>{
      this.setState({
          alert: {
              title: this.stran(title),
              message: this.stran(message),
              confirm_function: confirm_function ? () =>confirm_function():()=>this.hideAlert(),
              cancel_function: cancel_function ? () =>cancel_function():()=> this.hideAlert(),
              cancel_text: this.stran("Cancel"),
              confirm_text: this.stran("Ok"),
              showCancel: true,
              showConfirm: true,
              showProgress: false,
              show: true,
          }
      })
  }  
    showPropress = () => {
        this.setState({
            alert: {
                title: this.stran("Propressing"),
                message: this.stran("Waiting for a moment"),
                cancel_text: "",
                confirm_text: "",
                confirm_function: () => { },
                cancel_function: () => { },
                showCancel: false,
                showConfirm: false,
                showProgress: true,
                show: true,
            }
        })
    }  
hideAlert=()=>{
    this.setState({
        alert: {
            title: "",
            message: "",
            cancel_text: "",
            confirm_text: "",
            confirm_function: () => { },
            cancel_function: () => { },
            showCancel: false,
            showConfirm: false,
            showProgress: false,
            show: false,
        }})
}
    limitString = (string, num) => {

        if (typeof string == "string")
            return string.length > num ? string.slice(0, num) + "..." : string.slice(0, num);
    }
    getAllAddress = () => {
        return (
            <View style={{ flexDirection: "row", padding: 10, borderWidth: 1, margin: 3, borderColor: "blue" }}>
                <Image source={require("./../Images/icon-select.png")} style={{ width: 20, height: 20, resizeMode: "contain", marginRight: 3 }} />
                <View>
                    <Text>{this.stran("Ship to")}</Text>
                    <Text>{this.state.fullname} ,phone: {this.state.phone}</Text>
                    <Text>{this.state.address}</Text>
                </View>
            </View>
        )
    }
    getAddressData = (key, Data) => {
        this.setState({ [key]: Data })
    }
    showAddNewAdress = () => {

        Animated.timing(this.state.showAddAdress, {
            toValue: 0,
            duration: 500,
            easing: Easing.quad,
        }).start();
        Animated.timing(this.state.backshowAddAdress, {
            toValue: 1,
            duration: 500,
            easing: Easing.quad,
        }).start();

    }
    hideAddNewAdress = () => {
        Animated.timing(this.state.backshowAddAdress, {
            toValue: 0,
            duration: 500,
            easing: Easing.quad,
        }).start();
        Animated.timing(this.state.showAddAdress, {
            toValue: 800,
            duration: 500,
            easing: Easing.quad,
        }).start();
       

    }
    stran = (key) => {
        return stranl(this.state.Language, key);
    }

    render() {
        var stranl = this.stran;
        var lang = this.state.Language;

        const item = this.state.item


        const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } } };
        const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } } };

        return (
            <View style={styles.container}>
                <View style={styles.container_top_profile}>
                    <Text>{stranl("Ship & bill to")}</Text>
                    <View style={styles.address}>

                        <View style={{ color: "red", flexDirection: "row", justifyContent: "flex-end" }}>
                            <TouchableOpacity onPress={this.showAddNewAdress}>
                                <Text style={{ color: "red", fontWeight: "100", marginRight: 10 }}>{!this.state.fullname.length > 0 ? stranl("+ Add address") : stranl("+ Edit address")}</Text>
                            </TouchableOpacity>
                        </View>
                        <ScrollView>
                            {this.getAllAddress()}
                        </ScrollView>

                    </View>

                </View>
                <Image source={require("./../Images/border-letter.jpg")} style={{ width: WDevice, resizeMode: 'contain' }}></Image>
                <ScrollView>
                    <Text style={styles.container_top_profile}>{stranl("Package:")}</Text>
                    <FlatList
                        data={this.state.Cart}
                        extraData={this.props}
                        // refreshing={this.state.refresh}
                        // onRefresh={this.refresh}
                        keyExtractor={item => item.id}
                        renderItem={(item, rowID) => <CartFoodItem item={item} index={rowID} deleteCart={this.deleteCart} pagefor={"checkout"} />}
                    />
                </ScrollView>
                <View style={styles.btnOrder}>
                    <View style={{ flexDirection: "row", justifyContent: "center", alignItems: "center" }}>
                        <View style={{ marginLeft: 15, backgroundColor: "red", width: 10, height: 10 }}></View>
                        <View style={{ marginTop: 12 }}><Text style={{}}>  {stranl("Total: ") + this.state.totalItem + " items, " + this.state.totalMoney + " VND"}</Text>
                            <Text style={{ fontSize: 9, color: "gray" }}>  {this.state.savingMoney > 0 ? stranl("You are saving ") + this.state.savingMoney + " VND" : stranl("Are you ready to eat")}</Text>
                        </View>
                    </View>

                    <TouchableOpacity onPress={this.btnPlaceOrder}>
                        <View >

                            <Svg
                                height="50"
                                width="180"
                            >
                                <Text style={{ fontSize: 18, color: 'white', textAlign: "center", paddingTop: 14, marginLeft: 15 }}>{stranl("Place Order")}</Text>
                                <Defs>
                                    <LinearGradient id="grad" x1="0" y1="0" x2="180" y2="0">
                                        <Stop offset="0" stopColor="orange" stopOpacity="0.8" />
                                        <Stop offset="1" stopColor="#E94B3C" stopOpacity="1" />
                                    </LinearGradient>
                                </Defs>

                                <Polygon
                                    points="50,0 180,0 180,50 0,50 "
                                    fill="url(#grad)"

                                />

                            </Svg>

                        </View>
                    </TouchableOpacity>
                </View>
                {/* Address new form */}

                <Animated.View style={{ position: "absolute", flex: 1, backgroundColor: "#abafac9e", width: WDevice, height: HDevice, top: this.state.showAddAdress,opacity:this.state.backshowAddAdress }}>
                    <View style={{ backgroundColor: "white", width: WDevice, height: HDevice - 100, top: 100, borderRadius: 20 }}>
                        <View style={{ flexDirection: "row", justifyContent: "center", padding: 13, borderBottomWidth: 0.5, borderColor: "#dbe0db" }}>
                            <Text style={{ fontWeight: "bold", fontSize: 15 }}>{!this.state.fullname.length > 0 ? stranl("+ Add address") : stranl("+ Edit address")}</Text>

                        </View>

                        <View style={{ padding: 20 }}>
                            <TextField
                                label={stranl("Full name")}
                                value={this.state.fullname}
                                onChangeText={(data) => this.getAddressData("fullname", data)}
                            />
                            <TextField
                                label={stranl("Phone number")}
                                value={this.state.phone}
                                onChangeText={(data) => this.getAddressData("phone", data)}
                            />
                            <TextField
                                label={stranl("Address")}
                                value={this.state.address}
                                onChangeText={(data) => this.getAddressData("address", data)}
                            />

                            <Text>{this.state.location}</Text>

                        </View>
                        <TouchableOpacity style={{ position: "absolute", bottom: 60 }} onPress={()=>this.btnAddAddress()}>
                            <Svg
                                height="50"
                                width={WDevice}
                            >
                                <Text style={{ fontSize: 18, color: 'white', textAlign: "center", paddingTop: 14 }}>{stranl("Check Out")}</Text>
                                <Defs>
                                    <LinearGradient id="grad" x1="0" y1="0" x2="180" y2="0">
                                        <Stop offset="0" stopColor="orange" stopOpacity="0.8" />
                                        <Stop offset="1" stopColor="#E94B3C" stopOpacity="1" />
                                    </LinearGradient>
                                </Defs>

                                <Polygon
                                    points={`0,0 ${WDevice},0 ${WDevice},50 0,50`}
                                    fill="url(#grad)"

                                />

                            </Svg>
                        </TouchableOpacity>

                    </View>
                </Animated.View>
                <AwesomeAlert
                    show={this.state.alert.show}
                    showProgress={false}
                    title={this.state.alert.title}
                    message={this.state.alert.message}
                    closeOnTouchOutside={false}
                    showProgress={this.state.alert.showProgress}
                    closeOnHardwareBackPress={false}
                    showCancelButton={this.state.alert.showCancel}
                    showConfirmButton={this.state.alert.showConfirm}
                    cancelText={this.state.alert.cancel_text}
                    confirmText={this.state.alert.confirm_text}
                confirmButtonColor="green"
                    onCancelPressed={() => {
                        this.state.alert.cancel_function();
                    }}
                    onConfirmPressed={() => {
                        this.state.alert.confirm_function();
                    }}
                />
            </View>
        )
    }
}


const WDevice = Dimensions.get('window').width;
const HDevice = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: 'white',
        marginTop: 0,




    },
    container_top_profile: {

        paddingTop: 0,
        flexDirection: "row",
        backgroundColor: "white",
        flexDirection: "column",
        padding: 15
    },
    h2: {
        marginTop: 5,
        color: "black",
        fontSize: 17
    },
    input: {
        color: "gray",
        borderBottomWidth: 1,
        borderColor: "gray",
        marginLeft: 10,
        width: WDevice - 150,
        paddingBottom: 10
    },
    address: {
        width: WDevice - 30,
        height: 100,
        backgroundColor: "white",
        borderWidth: 1,
        borderColor: "red",
        borderStyle: "dotted"

    }, btnOrder: {
        backgroundColor: "white",
        width: WDevice,
        height: 50,
        flexDirection: "row",
        justifyContent: "space-between"
    },
})





const mapStateToProps = state => {
    return {
        Lang: state.Language,
        Cart: state.Cart,
    }
}


const mapDispatchToProps = (dispatch, props) => {
    return {

        UpdateCart: (result) => {

            dispatch(actionRedux.actUpdateCart(result));

        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckOut);