import * as Types from './../Constants/ActionTypes';
// import callApi from '../../Ultils/ApiCaller';
import {
    firebaseApp,
    db, storageRef
} from './../../utlils/firebaseConfig'

import ls from 'react-native-local-storage';
export const getBigCategory = function (cb) {

    var finalResult = [];
    db.collection('big-category').onSnapshot(snapshot => {
        var lang = localStorage.getItem("lang") || "en";
        var result = []
        snapshot.forEach(doc => {
            const {
                name_en,
                name_vi,
                description_en,
                description_vi,
                status
            } = doc.data();

            result.push({
                id: doc.id,
                name: eval("name_" + lang),
                description: eval("description_" + lang),
                status
            })
            cb(result);

        });

    });


}

export const getBigCategoryById = function (id, cb) {
    var finalResult = {};
    db.collection('big-category').doc(id + "").get().then(function (doc) {
        if (doc.exists) {
            cb(doc.data());
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });

}

export const adBigCategory = (bigcategory) => {
    console.log(bigcategory)
    db.collection("big-category").add({
            name_en: bigcategory.name_en,
            description_en: bigcategory.description_en,
            name_vi: bigcategory.name_vi,
            description_vi: bigcategory.description_vi,
            status: bigcategory.status
        })
        .then(function () {
            alert("Document successfully written!");
        })
        .catch(function (error) {
            alert("Error writing document: ", error);
        });
}



export const delBigCategory = (bigcategoryid) => {

    db.collection("big-category").doc(bigcategoryid).delete().then(function () {
        alert("Document successfully deleted!");
    }).catch(function (error) {
        alert("Error removing document: ", error);
    });

}

export const getImange = function (cb) {
    var finalResult = [];
    db.collection('image-library').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {
                name,
                status,
                img_size,
                img_url,
                create_date,
                content_type
            } = doc.data();
            result.push({
                id: doc.id,
                name,
                status,
                img_size,
                img_url,
                create_date,
                content_type
            })
            cb(result);

        });

    });


}
export const adImage = (image) => {
    db.collection("image-library").add({
            name: image.name,
            img_url: image.img_url,
            img_size: image.img_size,
            create_date: image.create_date,
            content_type: image.content_type,
            status: true
        })
        .then(function () {
            alert("Document successfully written!");
        })
        .catch(function (error) {
            alert("Error writing document: ", error);
        });
}


export const delImgageById = (id, cb) => {

    db.collection("image-library").doc(id).delete().then(function () {
        var desertRef = storageRef.ref(`images/${id}`)
        // Delete the file
        desertRef.delete().then(function () {
            // File deleted successfully
        }).catch(function (error) {
            // Uh-oh, an error occurred!
        });
        cb(true);
    }).catch(function (error) {
        cb(error);
    });

}

export const addMenu = (menu) => {
    console.log(menu)
    var today = new Date().toLocaleString();
    return db.collection("menu").add({
        name: menu.name,
        itemsMenu: menu.itemsMenu,
        slider: menu.slider,
        caption_drink_en: menu.caption_drink_en,
        caption_drink_vi: menu.caption_drink_vi,
        caption_food_en: menu.caption_food_en,
        caption_food_vi: menu.caption_food_vi,
        title_drink_en: menu.title_drink_en,
        title_drink_vi: menu.title_drink_vi,
        title_food_en: menu.title_food_en,
        title_food_vi: menu.title_food_vi,
        create_date: today,
        modified_by: "",
        modified_date: "",
        author: localStorage.getItem("uid"),
        view: 0,
        status: true

    })

}


export const getMenu = function (cb) {

    
      db.collection('menu').onSnapshot( snapshot => {
        // var lang   = localStorage.getItem("lang") || "en";
        var result = []
        
        snapshot.forEach(async doc => {
            var itemMenuDetail = [];
            const {
                name,
                itemsMenu,
                slider,
                caption_drink_en,
                caption_drink_vi,
                caption_food_en,
                caption_food_vi,
                title_drink_en,
                title_drink_vi,
                title_food_en,
                title_food_vi,
                create_date,
                modified_by,
                modified_date,
                author,
                view,
                status
            } = doc.data();
            console.log("-----------------")
            var promise1 = new Promise(function(resolve, reject) {  
                if(itemsMenu.length==0){
                    resolve()
                }
                var length=0;   
            itemsMenu.forEach(async (item) => {
              await     getProductByID(item, (re) => {
                length++;
                    itemMenuDetail.push(re)
                    if(itemsMenu.length==length){
                        resolve()
                    }

                })
            })
           
        })
        promise1.then(()=>{
     
                    result.push({
                        menu_id: doc.id,
                        name,
                        itemsMenu:itemMenuDetail,
                        slider,
                        caption_drink_en,
                        caption_drink_vi,
                        caption_food_en,
                        caption_food_vi,
                        title_drink_en,
                        title_drink_vi,
                        title_food_en,
                        title_food_vi,
                        create_date,
                        modified_by,
                        modified_date,
                        author,
                        view,
                        status
                    })
                    // console.log(result)
                    return cb(result);
                })
        });

    });


}

export const getMenuById = function (id,cb) {


    db.collection('menu').doc(id + "").onSnapshot(function (doc) {
        // var lang   = localStorage.getItem("lang") || "en";
        if(doc.exists){
        var result = {}

            var itemMenuDetail = [];
            const {
                name,
                itemsMenu,
                slider,
                caption_drink_en,
                caption_drink_vi,
                caption_food_en,
                caption_food_vi,
                title_drink_en,
                title_drink_vi,
                title_food_en,
                title_food_vi,
                create_date,
                modified_by,
                modified_date,
                author,
                view,
                status
            } = doc.data();
          
            // var promise1 = new Promise(function (resolve, reject) {
                if (itemsMenu.length == 0) {
                    // resolve()
                }
                var length = 0;
                itemsMenu.forEach(async (item) => {
                    await getProductByID(item, (re) => {
                        length++;
                        itemMenuDetail.push(re)
                        if (itemsMenu.length == length) {
                            // resolve()
                        }

                    })
                })

            // })
            // promise1.then(() => {

                result={
                    menu_id: doc.id,
                    name,
                    // itemsMenu: itemMenuDetail,
                    itemsMenu,
                    slider,
                    caption_drink_en,
                    caption_drink_vi,
                    caption_food_en,
                    caption_food_vi,
                    title_drink_en,
                    title_drink_vi,
                    title_food_en,
                    title_food_vi,
                    create_date,
                    modified_by,
                    modified_date,
                    author,
                    view,
                    status
                }
                // console.log(result)
                return cb(result);
            // })
        }else{
            console.log("No such menu!");
        }
    })

    

}

export const getMenuConfig = function (cb) {
    db.collection('menu-config').doc("1").onSnapshot(function (doc) {
        if (doc.exists) {
            data=doc.data();
            if (data.menu_config_type=="repeat_default"){
                getMenuById(data.menu_repeat_default,(menu)=>{
                    cb(menu)
                })
              
            }
        } else {
            // doc.data() will be undefined in this case
            console.log("No such document!");
        }
    })

}

export const adProduct = (product) => {

    var today = new Date().toLocaleString();
    return db.collection("product").add({
        name_en: product.name_en,
        name_vi: product.name_vi,
        big_category_id: product.BigCategoryID,
        description_en: product.description_en,
        description_vi: product.description_vi,
        img_url: product.img_url,
        meta_title: product.meta_title,
        meta_description: product.meta_description,
        meta_keyword: product.meta_keyword,
        create_date: today,
        modified_by: "",
        modified_date: "",
        author: localStorage.getItem("uid"),
        price: product.price,
        promotion_price: product.promotion_price,
        img_more: product.img_more,
        product_category_id: product.ProductCategoryID,
        ingredient_en: product.ingredient_en,
        nutritional_value_en: product.nutritional_value_en,
        ingredient_vi: product.ingredient_vi,
        nutritional_value_vi: product.nutritional_value_vi,
        currency: product.currency,
        view: 0,
        status: product.status

    })

}

export const getProduct = function (lang, cb) {
    var finalResult = [];
    db.collection('product').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {

                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                status
            } = doc.data();
            getBigCategoryById(big_category_id, (bigcategoryData) => {
                getProductCategoryByID(product_category_id, (productCategoryData) => {
                    getUserAdminByID(author, (userData) => {
                        if (bigcategoryData.status && productCategoryData.status) {
                            result.push({
                                id: doc.id,
                                bigCategoryName: eval("bigcategoryData.name_" + lang),
                                productCategoryName: eval("productCategoryData.name_" + lang),
                                authorName: userData.name,
                                name: eval("name_" + lang),
                                big_category_id,
                                description: eval("description_" + lang),
                                img_url,
                                meta_title,
                                meta_description,
                                meta_keyword,
                                create_date,
                                author,
                                price,
                                promotion_price,
                                img_more,
                                product_category_id,
                                ingredient: eval("ingredient_" + lang),
                                nutritional_value: eval("nutritional_value_" + lang),
                                currency,
                                status
                            })
                        }
                        cb(result);
                    })
                })

            })



        });

    });


}

export const getProductByProductCategoryId = function (id, cb) {
    var finalResult = [];
    db.collection('product').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {

                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                status
            } = doc.data();
            getBigCategoryById(big_category_id, (bigcategoryData) => {
                getProductCategoryByID(product_category_id, (productCategoryData) => {
                    getUserAdminByID(author, (userData) => {
                        if (bigcategoryData.status && productCategoryData.status) {
                            result.push({
                                id: doc.id,
                                bigCategoryName: bigcategoryData.name_en,
                                productCategoryName: productCategoryData.name_en,
                                productCategoryName_en: productCategoryData.name_en,
                                productCategoryName_vi: productCategoryData.name_vi,
                                authorName: userData.name,
                                name_en,
                                name_vi,
                                big_category_id,
                                description_en,
                                description_vi,
                                img_url,
                                meta_title,
                                meta_description,
                                meta_keyword,
                                create_date,
                                author,
                                price,
                                promotion_price,
                                img_more,
                                product_category_id,
                                ingredient_en,
                                nutritional_value_en,
                                ingredient_vi,
                                nutritional_value_vi,
                                currency,
                                status
                            })
                        }
                        cb(result);
                    })
                })

            })



        });

    });


}

export const getProductByIDLang = function (lang, id, cb) {
    var finalResult = [];

    db.collection('product').doc(id + "").onSnapshot((doc)=> {

        if (doc.exists) {

            var result = {};

            const {
                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                modified_by,
                modified_date,
                status
            } = doc.data();
            getBigCategoryById(big_category_id, (bigcategoryData) => {
                getProductCategoryByID(product_category_id, (productCategoryData) => {
                    getUserAdminByID(author, (userData) => {
                        var modified_ID = modified_by ? modified_by : "0";
                        getUserAdminByID(modified_ID, (userModifiedData) => {
                            if (bigcategoryData.status && productCategoryData.status) {
                                result = {
                                    id: doc.id,
                                    bigCategoryName: eval("bigcategoryData.name_" + lang),
                                    productCategoryName: eval("productCategoryData.name_" + lang),
                                    authorName: userData.name,
                                    name: eval("name_" + lang),
                                    big_category_id,
                                    description: eval("description_" + lang),
                                    img_url,
                                    meta_title,
                                    meta_description,
                                    meta_keyword,
                                    create_date,
                                    author,
                                    price,
                                    promotion_price,
                                    img_more,
                                    product_category_id,
                                    ingredient: eval("ingredient_" + lang),
                                    nutritional_value: eval("nutritional_value_" + lang),
                                    currency,
                                    modified_by,
                                    modified_by_name: userModifiedData.name,
                                    modified_date,
                                    status
                                }
                            }

                            cb(result);

                        })
                    })

                })



            });
        } else {
            cb(false);
        }
    });


}

export const getProductByID =async function (id, cb) {
    var finalResult = [];

    await db.collection('product').doc(id + "").onSnapshot((doc) => {

        if (doc.exists) {

            var result = {};

            const {
                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                modified_by,
                modified_date,
                status
            } = doc.data();
            getBigCategoryById(big_category_id, (bigcategoryData) => {
                getProductCategoryByID(product_category_id, (productCategoryData) => {
                    getUserAdminByID(author, (userData) => {
                        var modified_ID = modified_by ? modified_by : "0";
                        getUserAdminByID(modified_ID, (userModifiedData) => {
                            if (bigcategoryData.status && productCategoryData.status) {
                                result = {
                                    id: doc.id,
                                    bigCategoryName: bigcategoryData.name_en,
                                    productCategoryName: productCategoryData.name_en,
                                    product_category_name_vi: productCategoryData.name_vi,
                                    product_category_name_en: productCategoryData.name_en,
                                    product_category_img_url: productCategoryData.img_url,
                                    product_category_description_en: productCategoryData.description_en,
                                    product_category_description_vi: productCategoryData.description_vi,
                                    authorName: userData.name,
                                    name_en,
                                    name_vi,
                                    big_category_id,
                                    description_en,
                                    description_vi,
                                    img_url,
                                    meta_title,
                                    meta_description,
                                    meta_keyword,
                                    create_date,
                                    author,
                                    price,
                                    promotion_price,
                                    img_more,
                                    product_category_id,
                                    ingredient_en,
                                    nutritional_value_en,
                                    modified_by_name: userModifiedData.name,
                                    ingredient_vi,
                                    nutritional_value_vi,
                                    currency,
                                    modified_by,
                                    modified_date,
                                    status
                                }
                            }

                            cb(result);

                        })
                    })

                })



            });
        } else {
            cb(false);
        }
    });


}

export const searchProduct = function (lang, key, value, cb) {
    var finalResult = [];
    db.collection('product').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {

            const {
                name_en,
                name_vi,
                big_category_id,
                description_en,
                description_vi,
                img_url,
                meta_title,
                meta_description,
                meta_keyword,
                create_date,
                author,
                price,
                promotion_price,
                img_more,
                product_category_id,
                ingredient_en,
                nutritional_value_en,
                ingredient_vi,
                nutritional_value_vi,
                currency,
                modified_by,
                modified_date,
                status
            } = doc.data();
            // var obj[key]
            if (key == "name" || key == "description") {
                key = key + "_" + lang;
            }
            if (eval(key).toString().toLowerCase().includes(value.toLowerCase()))

                getBigCategoryById(big_category_id, (bigcategoryData) => {
                    getProductCategoryByID(product_category_id, (productCategoryData) => {
                        getUserAdminByID(author, (userData) => {
                            var modified_ID = modified_by ? modified_by : "0";
                            getUserAdminByID(modified_ID, (userModifiedData) => {
                                if (bigcategoryData.status && productCategoryData.status) {
                                    result.push({
                                        id: doc.id,
                                        bigCategoryName: eval("bigcategoryData.name_" + lang),
                                        productCategoryName: eval("productCategoryData.name_" + lang),
                                        authorName: userData.name,
                                        name: eval("name_" + lang),
                                        big_category_id,
                                        description: eval("description_" + lang),
                                        img_url,
                                        meta_title,
                                        meta_description,
                                        meta_keyword,
                                        create_date,
                                        author,
                                        price,
                                        promotion_price,
                                        img_more,
                                        product_category_id,
                                        ingredient: eval("ingredient_" + lang),
                                        nutritional_value: eval("nutritional_value_" + lang),
                                        currency,
                                        status
                                    });
                                }

                                cb(result);

                            })
                        })

                    })



                });


        });

    });


}


export const updateProduct = (id, product) => {
    console.log(product)
    var today = new Date().toLocaleString();
    return db.collection("product").doc(id).update({
        name_en: product.name_en,
        name_vi: product.name_vi,
        big_category_id: product.BigCategoryID,
        description_en: product.description_en,
        description_vi: product.description_vi,
        img_url: product.img_url,
        meta_title: product.meta_title,
        meta_description: product.meta_description,
        meta_keyword: product.meta_keyword,
        modified_date: today,
        modified_by: localStorage.getItem("uid"),
        price: product.price,
        promotion_price: product.promotion_price,
        img_more: product.img_more,
        product_category_id: product.ProductCategoryID,
        ingredient_en: product.ingredient_en,
        nutritional_value_en: product.nutritional_value_en,
        ingredient_vi: product.ingredient_vi,
        nutritional_value_vi: product.nutritional_value_vi,
        currency: product.currency,
        view: 0,
        status: product.status
    })

}

export const delProductByID = (id, cb) => {
    db.collection("product").doc(id).delete().then(function () {
        cb(true);
    }).catch(function (error) {
        cb(error);
    });

}

export const adProductCategory = (category) => {
    var today = new Date().toLocaleString();
    return db.collection("product-category").add({
        name_vi: category.name_vi,
        name_en: category.name_en,
        big_category_id: category.BigCategoryID,
        description_vi: category.description_vi,
        description_en: category.description_en,
        img_url: category.img_url,
        meta_title: category.meta_title,
        meta_description: category.meta_description,
        meta_keyword: category.meta_keyword,
        create_date: today,
        author: localStorage.getItem("uid"),
        status: category.status
    })

}
export const updateProductCategory = (id, category) => {
    console.log(category)
    var today = new Date().toLocaleString();
    return db.collection("product-category").doc(id).update({
        name_vi: category.name_vi,
        name_en: category.name_en,
        big_category_id: category.BigCategoryID,
        description_vi: category.description_vi,
        description_en: category.description_en,
        img_url: category.img_url,
        meta_title: category.meta_title,
        meta_description: category.meta_description,
        meta_keyword: category.meta_keyword,
        modified_date: today,
        modified_by: localStorage.getItem("uid"),
        status: category.status
    })

}

export const getProductCategoryByID = function (id, cb) {
    var finalResult = {};

    db.collection('product-category').doc(id + "").get().then(function (doc) {
        if (doc.exists) {
            cb(doc.data());
        } else {


            cb(false);
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    });
}

export const getProductCategoryByBigCategoryID = function (lang, BigCategoryID, cb) {
    var finalResult = [];
    console.log(BigCategoryID)
    db.collection('product-category').where("big_category_id", "==", BigCategoryID + "").where("status", "==", true).onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {
                id,
                name_en,
                description_en,
                name_vi,
                description_vi,
                status,
                big_category_id,
                author,
                create_date
            } = doc.data();

            getBigCategoryById(big_category_id, (data) => {
                // console.log(data.name)
                result.push({
                    id: doc.id,
                    name: eval("name_" + lang),
                    description: eval("description_" + lang),
                    bigCategoryName: data.name,
                    status,
                    status,
                    big_category_id,
                    author,
                    create_date
                })
                cb(result);
            })


        });

    });


}
export const getProductCategory = function (lang, cb) {
    var finalResult = [];
    db.collection('product-category').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {
            const {
                id,
                name_en,
                description_en,
                name_vi,
                description_vi,
                status,
                big_category_id,
                author,
                create_date
            } = doc.data();

            getBigCategoryById(big_category_id, (data) => {
                getUserAdminByID(author, (userData) => {
                    // console.log(data.name)
                    result.push({
                        id: doc.id,
                        name: eval("name_" + lang),
                        description: eval("description_" + lang),
                        bigCategoryName: eval("data.name_" + lang),
                        status,
                        big_category_id,
                        author_id: author,
                        author: userData.name,
                        create_date
                    })
                    console.log(result)
                    cb(result);
                })
            })


        });

    });


}

export const searchProductCategory = function (lang, key, value, cb) {
    var finalResult = [];
    db.collection('product-category').orderBy("create_date", "desc").onSnapshot(snapshot => {
        var result = []
        snapshot.forEach(doc => {

            const {
                id,
                name_en,
                description_en,
                name_vi,
                description_vi,
                status,
                big_category_id,
                author,
                create_date
            } = doc.data();
            // var obj[key]
            if (key == "name" || key == "description") {
                key = key + "_" + lang;
            }

            if (eval(key).toString().toLowerCase().includes(value.toLowerCase()))
                getBigCategoryById(big_category_id, (data) => {
                    // console.log(data.name)
                    result.push({
                        id: doc.id,
                        name: eval("name_" + lang),
                        bigCategoryName: data.name,
                        status,
                        description: eval("description_" + lang),
                        big_category_id,
                        author,
                        create_date
                    })
                    cb(result);
                })


        });

    });


}


export const delProductCategory = (id, cb) => {

    db.collection("product-category").doc(id).delete().then(function () {
        cb(true);
    }).catch(function (error) {
        cb(error);
    });

}




export const checkLogin = function (username, password, cb) {
    var finalResult = [];
    var result = false;
    var userExist = false;
    var keycheck =username.includes("@")?"email":"username";
    db.collection('userCustomer').where(keycheck, "==", username).onSnapshot(snapshot => {
        snapshot.forEach(doc => {
            userExist = true;
            if (doc.exists) {

                firebaseApp.auth().signInWithEmailAndPassword(doc.data().email, password).then((user) => {
                    result = true;
                    ls.save('email', doc.data().email)
                        .then(() => {
                        })
                    cb(true);


                }).catch(function (error) {
                   
                    cb(false)
                
                });
            } else {
                cb(false);
            }
        })

        if (!userExist) cb(false)
    })


}

export const getUserAdminByID = function (id, cb) {
    var finalResult = {};

    db.collection('UserAdmin').where("user_id", "==", id).onSnapshot(snapshot => {
        var exist = false;
        snapshot.forEach(doc => {
            exist = true;
            if (doc.exists) {
                cb(doc.data());
            } else {


                cb(false);
            }
        })
        if (exist == false) cb(false);
    })
}

export const getAllUserCustomerCondition= function (email, cb) {
    var finalResult = {};

    db.collection('userCustomer').where("email", "==", email).onSnapshot(snapshot => {
        var exist = false;
        snapshot.forEach(doc => {
            exist = true;
            if (doc.exists) {
                cb(doc.id,doc.data());
            } else {


                cb(false);
            }
        })
        if (exist == false) cb(false);
    })
    }
    export const getCurrentUserCustomer=(cb)=>{
        ls.get('email').then((email) => {

            if (email.trim().length > 0) {
                getAllUserCustomerCondition(email, (id, result) => {
                    if (result != false) {
                        db.collection('userCustomer').doc(id + "").onSnapshot((doc) => {

                            if (doc.exists) {

                                cb(id,doc.data())
                            }else{
                                cb(false)
                            }
                        })
                    }
                })


            } else {
                    cb(false)
            }

        }).catch((e) => {
            alert("login fail", e)
        });

    }
    export const updateAddAddress=(address,cb)=>{
        ls.get('email').then((email) => {
           var once =true;
            if (email.trim().length > 0) {
                getAllUserCustomerCondition(email,(id,result)=>{
                    if(once){
                    once=false;
                    if(result!=false){
                        db.collection("userCustomer").doc(id+"").update({
                        address:address
                    }).then(() => {
                        cb(true)
                    }).catch(e => { console.log(e) })
                }}
            })
            
          
        } else {

        }

    }).catch((e) => {
        alert("login fail",e)
    });

}
const createUserCustomerOnDB = function (username, email) {
    var today = new Date().toLocaleString();
    return db.collection("userCustomer").add({
        name:"",
        username,
        email,
        user_profile:"",
        address:"",
        phone:"",
        create_date: today,
        status:true
    })
}
const checkUserNameExist=(username,cb)=>{
    db.collection('big-category').where("username","==",username).onSnapshot(snapshot => {
       var result=false;
        snapshot.forEach(doc => {
            if(doc.exists){
              result=true;
            }
        })
        cb(result)
    })
}
export const createUserCustomer = function (username,email,password, cb) {
    checkUserNameExist(username,(is)=>{
   if(is){
        cb("username existed!")
    }else{
firebaseApp.auth().createUserWithEmailAndPassword(email, password)
    .then(function success(userData) {
        // var uid = userData.uid; // The UID of recently created user on firebase
        createUserCustomerOnDB(username,email).then(()=>{
            cb(true)
        }).catch((e)=>{
            cb(e)
        })
        // var displayName = userData.displayName;
        // var email = userData.email;
        // var emailVerified = userData.emailVerified;
        // var photoURL = userData.photoURL;
        // var isAnonymous = userData.isAnonymous;
        // var providerData = userData.providerData;

    }).catch(function failure(error) {

        // console.log(errorCode + " " + errorMessage);
        cb(error )

    });
    }
    })
}


export const addOrder = (fullname, phone, address, CartComponentPrice,Cart, totalItem,totalMoney, savingMoney,cb)=>{
    if (typeof fullname != "undefined", typeof phone != "undefined", typeof fullname != "undefined", typeof phone != "undefined", typeof address != "undefined", typeof CartComponent != "undefined", typeof Cart != "undefined", typeof totalItem != "undefined", typeof totalMoney != "undefined", typeof savingMoney != "undefined", typeof address != "undefined", typeof CartComponent != "undefined", typeof Cart != "undefined", typeof totalItem != "undefined", typeof totalMoney != "undefined", typeof savingMoney != "undefined"){
    var today = new Date().toLocaleString();
        var send = { fullname, phone, address, Cart, totalItem, totalMoney, savingMoney}
        ls.get('email').then((email) => {

            if (email.trim().length > 0) {
                var { fullname, phone, address, Cart, totalItem, totalMoney, savingMoney }=send;
 
                db.collection("Order").add({
                usermail: email,
                fullname, 
                phone,  
                address, 
                    CartComponentPrice,
                 Cart, 
                    Acceptance:Cart, 
                 totalItem,
                  totalMoney, 
                  savingMoney,
                received_state: false,
                cancel_state: false,
                centent_cancel: "",
                delivered_state: false,
                finished_state: false,
                create_date: today,
                status: true
            }).then(()=>{cb(true)}).catch(()=>{console.log(e);cb(false)})
        }else{
         
            cb(false)
        }}).catch((e)=>{
           
            console.log(e)
            cb(false)
        })
        
    }else{
       
        cb(false)
    }
       
}
// export const actFetchBigCategoryRequest = () => {

//    var jo=   (dispatch) => {

//             db.collection('big-category').get().then(snapshot => {
//                 var result = []
//                 snapshot.forEach(doc => {

//                     const { name, status } = doc.data();

//                     result.push({
//                         id: doc.id,
//                         name: name, status
//                     })
//                     dispatch(actFetchCategory(result));

//                 });
//             });


//     }

//     return jo(dispatch)
// }

export const updateLanguage = (Language) => {

    return {
        type: Types.FETCH_LANGUAGE,
        Language
    }
}

export const actFetchBigCategory = (BigCategory) => {

    return {
        type: Types.FETCH_BIGCATEGORY,
        BigCategory
    }
}

export const actFetchProductCategory = (ProductCategory) => {

    return {
        type: Types.FETCH_PRODUCTCATEGORY,
        ProductCategory
    }
}


export const actFetchHotelsRequest = () => {
    return (dispatch) => {
        return callApi('hotel', 'GET', null).then((res) => {

            dispatch(actFetchHotels(res.data));

        });
    }
}

export const actFetchHotels = (hotels) => {
    return {
        type: Types.FETCH_HOTELS,
        hotels
    }
}

export const actFetchCart = (Cart) => {
    return {
        type: Types.FETCH_CART,
        Cart
    }
}
export const actUpdateCart = (Cart) => {
    return {
        type: Types.UPDATE_CART,
        Cart
    }
}


// export const actFetchBigCategoryRequest = ()=>{
//     return (dispatch)=>{
//         return apiBigCategory('hotel','GET', null).then((res)=>{

//             dispatch(actFetchCategory(res.data));

//         });
//     }
// }

// export const actFetchCategory = (hotels)=>{
//     return {
//         type: Types.FETCH_HOTELS,
//         hotels
//     }
// }