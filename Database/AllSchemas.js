
import Realm from 'realm';

export const UserSchema ="UserSchema"
export const InfoSchema = "InfoSchema"

export const IUserSchema={
    name:UserSchema,
    primaryKey: 'id',
    properties:{
        id:'int',
        user_name:{type:'string',indexed:true},
        status:{type:'bool',default:true}
    }
}

export const IInfoSchema= {
    name: UserSchema,
    primaryKey: 'id',
    properties: {
        id: 'int',
        user_name: { type: 'string', indexed: true },
        status: { type: 'bool', default: true },
        user: { type: 'list', objectType: UserSchema}
    }
}
const databaseOption ={
    path:'koredb.realm',
    schema: { IUserSchema, IInfoSchema},
    schemaVersion:0
}
export const insertNewUser = newUser => new  Promise((resolve,reject) => {
Realm.open(databaseOption).then(realm=>{
realm.write(()=>{
    realm.create(UserSchema,newUser);
    resolve(newUser)
})
}).catch((err)=>{reject(err)})
})

export const updateUser = User =>new Promise(( resolve, reject ) => {
    Realm.open(databaseOption).then(realm => {
        realm.write(() => {
           let update= realm.objectForPrimaryKey(UserSchema,User.id)
            update.name = User.name;
           resolve()
        })
    }).catch((err) => { reject(err) })
})

export const deleteUser = User =>new Promise((resolve, reject)=> {
    Realm.open(databaseOption).then(realm => {
        realm.write(() => {
            let userdel = realm.objectForPrimaryKey(UserSchema, User.id)
            realm.delete(userdel)
            resolve()
        })
    }).catch((err) => { reject(err) })
})

export const deleteAllUser = () => new Promise((resolve, reject) => {
    Realm.open(databaseOption).then(realm => {
        realm.write(() => {
            let userAlldel = realm.object(UserSchema);
            realm.delete(userAlldel)
            resolve()
        })
    }).catch((err) => { reject(err) })
})

export const queryAllUser = () => new Promise((resolve, reject) => {
    Realm.open(databaseOption).then(realm => {
       
            let user = realm.object(UserSchema);
            
            resolve(user)
      
    }).catch((err) => { reject(err) })
})
export default new Realm(databaseOption);